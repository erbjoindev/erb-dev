Ext.define('ExtApp.view.textfield.BasicTextfield',{
    extend: 'Ext.form.field.Text',
    alias: 'widget.basictextfield',
    enforceMaxLength:true,
    msgTarget: 'qtip'
    // , fieldStyle : 'text-transform: uppercase' 
    , initComponent: function(){
        var me = this;
        me.baseLabel = me.fieldLabel + ":";
        me.baseFieldStyle = me.fieldStyle;

        if(me.textAlign=='right'){
            me.textAlign='text-align:right;';
        }else{
            me.textAlign='text-align:left;';
        }

        if(me.readOnly==true){
            me.fieldStyle='background-color: #ddd; background-image: none;' + ' ' + me.textAlign;
        }else{
            me.fieldStyle=me.textAlign;
        };

        if(me.allowBlank==false){
            me.afterLabelTextTpl=ExtApp.util.Util.required;
        }else{
            me.afterLabelTextTpl='';
        };


        me.callParent(arguments);
    }, 
    isetAllowBlank:function(pMode){
        var me = this;
        if(pMode===false){
            me.labelEl.update(me.baseLabel + ExtApp.util.Util.required);
        }else{
            me.labelEl.update(me.baseLabel);
        };

        Ext.apply(me,{allowBlank: pMode}, {});
    },
    isetReadOnly:function(pMode){
        var me = this;
        if(pMode==false){
            me.setFieldStyle('background-color: #ffffff; background-image: none;' + ' ' + me.textAlign);
        }else{
            me.setFieldStyle('background-color: #ddd; background-image: none;' + ' ' + me.textAlign);
        };
        me.setReadOnly(pMode);
    }
    , enableKeyEvents: true
    , onKeyUp: function(e, o){
        var value = this.getValue().toUpperCase();
        this.setValue(value);
        this.fireEvent('keyup', this, e);
    }
});