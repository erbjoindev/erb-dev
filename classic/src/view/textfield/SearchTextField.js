Ext.define('ExtApp.view.textfield.SearchTextField',{
	extend: 'ExtApp.view.textfield.BasicTextfield',
	alias: 'widget.searchtextfield',

	requires: [
		'Ext.tip.ToolTip' 
	],

	initComponent: function(){
		var me = this;

		me.listeners= {
			render: function(p) {                                            
	            Ext.create('Ext.tip.ToolTip', {
	                target: me.id,
	                anchor: 'top',
	                anchorOffset: 85,
	                html: 'Enter keyword to search...<br><br>' +
	                'You can use <strong>%</strong> to represent one or more characters.<br>' +
	                'Example: <br>' +
	                'Keyword: %COLA%<br>' +
	                'Result: ' +
	                '<ul>' +
	                '<li>PEPSI <strong>COLA</strong> CAN 330 ML</li>' +
	                '<li>PEPSI <strong>COLA</strong> PET 1.5LT</li>' +
	                '<li>COCA <strong>COLA</strong> DIET CAN 330 ML X 4</li>' +
	                '<li>AJE BIG <strong>COLA</strong> 535ML`</li>' +
	                '<li>CAPUCINI CHO<strong>COLA</strong>TE SLIM T/P 200 ML</li>' +
	                '</ul>'
	            });
	        }
	    }


		me.callParent(arguments);
	}


});