Ext.define('ExtApp.view.fieldset.BasicFieldset', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.basicfieldset',
    collapsible : true,
    autoScroll: true,
    layout: {
        type: 'anchor'
    },    
    msgTarget: 'side',
    bodyPadding : 5,    
    initComponent: function(){
        var me = this;
        me.callParent(arguments);
    }
});