Ext.define('ExtApp.view.lov.LovStore',{
	statics: {
		getStore: function(pLovType){
			switch(pLovType){
				case 'user':
                	return "ExtApp.store.security.User";
                	break;               	
				case 'profile':
                    return "ExtApp.store.security.Profile";
                    break;
				case 'paramheader':
					return 'ExtApp.store.security.ParameterTableHdr';
					break;
			}
		}
	}
});