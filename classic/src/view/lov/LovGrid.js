Ext.define('ExtApp.view.lov.LovGrid',{
	extend: 'Ext.grid.Panel',
	alias: 'widget.lovgrid',
	requires: [
		'ExtApp.view.lov.LovColumn',
		'ExtApp.view.lov.LovStore',
        
        "ExtApp.store.security.User",
        "ExtApp.store.security.Profile",
        'ExtApp.store.security.ParameterTableHdr'
	],
	initComponent: function() {
        var me= this;
        
        me.store = Ext.create(ExtApp.view.lov.LovStore.getStore(me.lovType));
        me.columns= ExtApp.view.lov.LovColumn.getColumn(me.lovType);

        me.autoScroll = true;        
        me.frame = true;
        me.enableColumnMove = false;
        me.enableColumnHide = false;        
        me.columnLines = true;        
        
        me.viewConfig= {
            stripeRows: true
        };

        if(me.lovOrList=='list'){
            me.selModel= {
                selType: 'checkboxmodel'
            };
        }else{
            me.selModel= {
                selType: 'rowmodel'
            };
        };

        me.dockedItems = [{
            xtype: 'pagingtoolbar'
            , store: me.store
            , displayInfo: false 
            , displayMsg: 'Displaying records {0} - {1} of {2}'
            , emptyMsg: "No records to display"
            , dock: 'bottom'
            , listeners : {
                beforechange : function(){
                    me.store.proxy.extraParams =
                    {
                        lovSearch : me.initValue,
                        lovSearch2 : me.lovSearch2,
                        lovSearch3 : me.lovSearch3,
                        lovSearch4 : me.lovSearch4                
                    };
                }
            }
        }];
        me.loadStore();
        me.callParent(arguments);
    },

    loadStore: function(){
        var me = this;
        if(me.lovOrList=='lov'){
            me.store.load({
                params : {
                    lovSearch : me.initValue,
                    lovSearch2 : me.lovSearch2,
                    lovSearch3 : me.lovSearch3,
                    lovSearch4 : me.lovSearch4                
                }
            });
        }else{
            me.store.load({
                params : {
                    lovSearch2 : me.lovSearch2,
                    lovSearch3 : me.lovSearch3,
                    lovSearch4 : me.lovSearch4                  
                },
                callback: function(records, operation, success){
                    var vcounter = 0;    
                    var source = me.initValue+",";
                    var vColumnName=me.columns[0].dataIndex;
                    me.store.each(function(record){
                        if(source.indexOf(record.get(vColumnName)+",")>=0){
                            me.selModel.select(record.index, true);                                   
                        };
                        vcounter++;
                    });
                }                 
            });
        }

    }	
});