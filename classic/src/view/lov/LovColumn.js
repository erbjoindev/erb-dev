Ext.define('ExtApp.view.lov.LovColumn',{
	statics: {
		getColumn: function(pLovType){
			switch(pLovType){
                case "profile" : 
                    return [
                        {
                            text: "Profile Code", 
                            dataIndex: 'mupf_code', 
                            width: 100,
                            align: 'left',
                            sortable: false
                        },
                        {
                            text: "Profile Name", 
                            dataIndex: 'mupf_name', 
                            width: 250,
                            align: 'left',
                            sortable: false
                        }
                    ];
                    break; 
                case "user":
                    return [
                        {
                            text: "User Name", 
                            dataIndex: 'musr_code', 
                            width: '50%',
                            align: 'left',
                            sortable: false
                        },
                        {
                            text:"Full Name",
                            dataIndex:'musr_name',
                            width: '50%',
                            align:'left',
                            sortable:false

                        }
                    ];
                    break;
                case 'paramheader':
                    return[
                    {
                        header:'Table No',
                        dataIndex:'pthd_no',
                        width:100,
                        align:'left'
                    },
                    {
                        header:'Table Description',
                        dataIndex:'pthd_desc',
                        width:250,
                        align:'left'
                    }
                    ];
                    break;
			}
		}
	}
});