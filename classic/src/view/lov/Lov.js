Ext.define('ExtApp.view.lov.Lov',{
	extend: 'Ext.window.Window',
	alias: 'widget.lov',
	requires: [
		'ExtApp.view.button.ButtonRefresh',
		'ExtApp.view.button.ButtonCancel',
		'ExtApp.view.button.ButtonOk',
		'ExtApp.view.lov.LovGrid',
        'Ext.tip.ToolTip' 
	],
	layout: {
		type: 'fit'
	},	
    height : 450,
    width : 400,
    bodyPadding: 5,
    glyph: 'f29c@FontAwesome',
    title: 'Search',    
    lovType: '',
    lovSearch2: '',
    lovSearch3: '',
    lovSearch4: '',
    lovSearch5: '',
    lovOrList: 'lov',

    initComponent: function(application) {        
    	var me = this;
        if (me.initField !== undefined) {
            var vInitField = me.initField.getValue();
        } else {
            var vInitField = "";
        }

        me.closable = false;
        me.modal = true;

        if(me.lovOrList=='lov'){
            me.dockedItems = [
                {
                    xtype: 'toolbar',
                    dock : 'top',
                    layout : 'anchor',
                    defaults : {
                        margin: 5
                    },
                    items : [
                        {
                            xtype: 'fieldcontainer',
                            fieldLabel: 'Search Criteria',
                            layout: 'hbox',
                            anchor : '100%',                            
                            items: [
                                {
                                    xtype : 'textfield',
                                    itemId : 'searchCriteria',
                                    id: 'searchCriteria',
                                    value : vInitField,
                                    
                                    listeners: {
                                        specialkey: function(field, e, options){
                                            if (e.getKey() == e.ENTER){                                            
                                                var button = me.down('buttonrefresh');
                                                //button.handler();
                                                button.fireEvent('click',button);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'tbspacer',
                                    width: 5
                                }, 
                                {
                                    xtype : 'buttonrefresh',
                                    listeners: {
                                        click: function(){
                                            var fieldSearch = me.down("#searchCriteria");
                                            var grid=me.down('lovgrid');
                                            grid.initValue=me.down("#searchCriteria").getValue();
                                            grid.loadStore();
                                            // grid.focus();
                                        }
                                    }
                                }
                            ] 
                        }
                    ]
                }
            ];
        }; 

        me.items = [
            {
                xtype : 'lovgrid',
                lovType : me.lovType,
                initValue : vInitField,
                lovSearch2 : me.lovSearch2,
                lovSearch3 : me.lovSearch3,
                lovSearch4 : me.lovSearch4,
                lovSearch5 : me.lovSearch5,
                lovOrList : me.lovOrList, 
                listeners: {
                    celldblclick : function(){
                        var button = me.down('buttonok');
                        button.handler();
                    }
                } 
            }
        ];

        me.buttons = [            
            {
                xtype: 'buttonok',
                handler : function(){
                    var vGrid=me.down('lovgrid');
                    if(vGrid.getSelectionModel().getSelection().length > 0){

                        if(me.lovOrList=='lov'){
                            var vRecord=vGrid.getSelectionModel().getSelection()[0];    
                            var vColumnName=vGrid.columns[0].dataIndex;
                            if(me.useGrid){
                                var gridSelected=me.gridObj.getSelectionModel().getSelection()[0];
                                var cellPosition=me.gridObj.getSelectionModel().getCurrentPosition();
                                gridSelected.set(me.targetField,vRecord.get(vColumnName));
                                me.gridObj.plugins[0].startEditByPosition(
                                    cellPosition
                                );
                             }else{
                                me.targetField.setValue(vRecord.get(vColumnName));
                                me.targetField.focus(false,200);
                             }                            
                        }else{
                            var vRecord=vGrid.getSelectionModel().getSelection();    
                            var vResult='';
                            var vCounter=0;
                            Ext.each(vRecord,function(item){
                                var vColumnName=vGrid.columns[0].dataIndex;
                                vCounter=vCounter+1;
                                if(vCounter==1){
                                    vResult=vResult+item.data[vColumnName];    
                                }else{
                                    vResult=vResult+','+item.data[vColumnName];    
                                }
                                
                            });
                            me.targetField.setValue(vResult);    
                            me.targetField.focus(false,200);
                        };
                        me.close();    


                    }
                }
            },
            {
                xtype: 'buttoncancel',
                handler : function(){
                    me.close();
                }
            }
        ];

        me.listeners={
            show : function(){
                if(me.lovOrList=='lov'){
                    me.down('#searchCriteria').focus(false,200);
                }
            }
        };
    	
        me.callParent(arguments); 
    }
});