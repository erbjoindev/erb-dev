Ext.define('ExtApp.view.form.BasicForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.basicform',
    layout: {
        type: 'fit'
    },    
    border : 0,
    bodyPadding : 2,
    initComponent: function(){
        var me = this;
        me.callParent(arguments);
    },
    isetReadOnly:function(mode){
        // setreadonly for textfield
        var me=this;
        me.query('[xtype^=textfield]').forEach(
            function(c){
                        c.isetReadOnly(mode);
            }
        );

        me.query('[xtype^=trigger]').forEach(
            function(c){
                        c.isetReadOnly(mode);
            }
        );

        me.query('[xtype^=textarea]').forEach(
            function(c){
                        c.isetReadOnly(mode);
            }
        );

        me.query('[xtype^=combo]').forEach(
            function(c){
                        c.isetReadOnly(mode);
            }
        );
    }    
});