Ext.define('ExtApp.view.fieldcontainer.BasicFieldcontainer', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.basicfieldcontainer',
    msgTarget: 'side',
    layout: 'hbox',
    hideLabel: true,
    // maxWidth : 400,
    // minWidth : 200,
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
    }
});
