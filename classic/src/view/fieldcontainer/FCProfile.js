Ext.define('ExtApp.view.fieldcontainer.FCProfile', {
    extend: 'ExtApp.view.fieldcontainer.BasicFieldcontainer',
    alias: 'widget.fcprofile'
    ,requires:[
            'ExtApp.view.trigger.TriggerProfile'
	        ,'ExtApp.view.textfield.BasicTextfield'
    ]
    ,hideLabel : true
    ,initComponent: function() {
        var me = this;
        if(me.iallowBlank==undefined){
        	me.iallowBlank=true;
        }
        if(me.ireadOnly==undefined){
        	me.ireadOnly=false;
        }
        if(me.iwidth==undefined){
            me.iwidth = 200;
        }

        if(me.ieditable==undefined){
        	me.ieditable = true;
        }

        if(me.inameCode==undefined){
        	me.inameCode = 'mupf_code';
        }

        if(me.inameDesc==undefined){
        	me.inameDesc = 'mupf_name';
        }


	    me.items = [
            {
    	        xtype : 'triggerprofile'
    	        ,width : me.iwidth
    	        ,name : me.inameCode
                ,value : me.ivalueCode
                ,labelAlign: me.iLabelAlign            
    	        ,msgTarget: 'qtip'
    	        ,allowBlank : me.iallowBlank
    	        ,readOnly : me.ireadOnly
                ,editable : me.ieditable
    	        ,listeners: {
                    'blur' : function (textfield) {
                        if (textfield.getValue()==''){
                            textfield.next().next().setValue('');
                        } else {
                            ExtApp.util.Util.getDataAsync('profile',textfield.getValue(),'','',textfield.next().next(), 'mupf_name' );
                        }
                    }
                }
    	    }
            ,{
    	        xtype : 'tbspacer'
    	        ,width : 5
    	    }
            ,{
    	        xtype : 'basictextfield'
    	        ,name : me.inameDesc
    	        ,readOnly : true
    	        ,hideLabel : true
    	        ,width: 200
    	    }
        ];

        me.callParent(arguments);
    }
});

