Ext.define('ExtApp.view.combobox.ComboActive',{
	extend: 'Ext.form.field.ComboBox',
	alias: 'widget.comboactive',
	itemId: 'comboactive',	
	msgTarget: 'qtip',
	initComponent: function(){
		var me = this;
		me.store = Ext.create('Ext.data.Store',{
			fields: [
				{name: 'act_code', type: 'int'},
				{name: 'act_name'}
			],

			data: [
				{'act_code': 1, 'act_name': 'Yes'},
				{'act_code': 0, 'act_name': 'No'}
			]
		});
		me.fieldLabel = 'Active';		
		me.queryMode = 'local';
		me.displayField= 'act_name';
		me.valueField = 'act_code';	
	    // me.labelWidth=50;	
		me.maxWidth=180 ;
		me.minWidth=180	;			
		me.forceSelection= true
		if(me.allowBlank==false){
			me.afterLabelTextTpl= ExtApp.util.Util.required
		} else {
			me.afterLabelTextTpl ='';
		}
		me.callParent(arguments);
	},
    isetAllowBlank:function(pMode){
        var me = this;
        if(pMode===false){
            me.labelEl.update(me.baseLabel + ExtApp.util.Util.required);
        }else{
            me.labelEl.update(me.baseLabel);
        };

        Ext.apply(me,{allowBlank: pMode}, {});
    },
    isetReadOnly:function(pMode){
        var me = this;
        if(pMode==false){
            me.setFieldStyle('background-color: #ffffff; background-image: none;' + ' ' + me.textAlign);
        }else{
            me.setFieldStyle('background-color: #ddd; background-image: none;' + ' ' + me.textAlign);
        };
        me.setReadOnly(pMode);
    }	
});