Ext.define('ExtApp.view.combobox.BasicCombobox', {
    extend: 'Ext.form.field.ComboBox',
    requires: [
        'ExtApp.store.referential.ComboGeneral'
    ],
    alias: 'widget.basiccombobox',
    msgTarget: 'side',
    // editable:false,
    maxWidth : 400,
    minWidth : 200,
    selectOnFocus: false,
    forceSelection: true,
    matchFieldWidth:false,
    valueField:'code',
    displayField: 'code',
    listConfig:
    {
        loadingText: 'Searching...',
        emptyText: 'No results found',
        width : 300
    },    
    initComponent: function() {
        var me = this;
        me.baseLabel = me.fieldLabel + ":";
        if(me.readOnly==true){
            me.fieldStyle='background-color: #ddd; background-image: none;';
            me.hideTrigger=true;
        };
        if(me.allowBlank==false){
            me.afterLabelTextTpl=ExtApp.util.Util.required;
        }else{
            me.afterLabelTextTpl='';
        };

        me.store = Ext.create('ExtApp.store.referential.ComboGeneral');
        if(me.iautoLoad){
            me.store.load({
                 params:{
                     CBO: me.comboType,
                     key1:ExtApp.util.Util.remUndefined(me.key1), 
                     key2:ExtApp.util.Util.remUndefined(me.key2), 
                     key3:ExtApp.util.Util.remUndefined(me.key3)
                    }
            });         
        }       
        me.queryMode='local';
        me.mode='local';
        me.triggerAction= 'all';
        me.typeAhead=true;

        if(me.noDescription){
            me.displayTpl= Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{code}',
                '</tpl>'
            );
            me.tpl= Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">',
                        '<div >{code}-{description}</div>',  
                     '</div>',     
                '</tpl>'
            );
        }else if(me.noCode){
            me.displayTpl= Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{description}',
                '</tpl>'
            );
            me.tpl= Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">',
                        '<div >{description}</div>',
                     '</div>',     
                '</tpl>'
            );
        }else{    
            me.displayTpl= Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '{code}-{description}',
                '</tpl>'
            );
            me.tpl= Ext.create('Ext.XTemplate',
                '<tpl for=".">',
                    '<div class="x-boundlist-item">',
                        '<div >{code}-{description}</div>',      
                     '</div>',     
                '</tpl>'
            );
        }
        // };
        // me.displayField = 'code';
        this.callParent(arguments);
    },
    iloadStore:function(){
        var me = this;
        me.store.removeAll();
        me.store.load({
             params:{
                 CBO: me.comboType,
                 key1:me.key1, 
                 key2:me.key2, 
                 key3:me.key3
                }
        });
    },
    isetAllowBlank:function(pMode){
        var me = this;
        if(pMode===false){
            me.labelEl.update(me.baseLabel + ExtApp.util.Util.required);
        }else{
            me.labelEl.update(me.baseLabel);
        };

        Ext.apply(me,{allowBlank: pMode}, {});
    },
    isetReadOnly:function(pMode){
        var me = this;
        if(pMode==false){
            me.setFieldStyle('background-color: #ffffff; background-image: none;');

        }else{
            me.setFieldStyle('background-color: #ddd; background-image: none;');
        };
        me.setReadOnly(pMode);
        me.hideTrigger = pMode;
        me.updateLayout();
    },
    isetParam:function(pKey1, pKey2, pKey3){
        var me = this;
        me.key1 = pKey1;
        me.key2 = pKey2;
        me.key3 = pKey3;

    }
});
