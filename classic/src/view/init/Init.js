Ext.define('ExtApp.view.init.Init',{
	extend: 'Ext.panel.Panel',
	alias: 'widget.app-init',
	requires: [
		'Ext.plugin.Viewport',
		'ExtApp.view.login.Login',
		'ExtApp.view.main.Main',
		'ExtApp.util.Util',
		'ExtApp.util.Alert',
		'ExtApp.util.Vars'
	],
	layout: {
		type: 'fit' 
	},
	initComponent: function(){
		Ext.tip.QuickTipManager.init();     

		// custom vtype for vtype: 'numberonly'
		var numberonlyTest = /^[0-9]/i;
		Ext.apply(Ext.form.field.VTypes, {
		    //  vtype validation function
		    numberonly: function(val, field) {
		        return numberonlyTest.test(val);
		    },
		    // vtype Text property: The error text to display when the validation function returns false
		    numberonlyText: 'Not a valid number',
		    // vtype Mask property: The keystroke filter mask
		    numberonlyMask: /^[0-9]/i
		});

		// custom Vtype for vtype:'daterange'

		Ext.apply(Ext.form.VTypes, {
            daterange : function(val, field) {
                var date = field.parseDate(val);

                if(!date){
                    return;
                }
                if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                    var start = field.parent.down(field.startDateField);
                    start.setMaxValue(date);
                    start.validate();
                    this.dateRangeMax = date;
                } 
                else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                    var end = field.parent.down(field.endDateField);
                    end.setMinValue(date);

                    end.validate();
                    this.dateRangeMin = date;
                }
                return true;
            }
        });

		 // custom Vtype for vtype:'time'
		var timeTest = /^([1-9]|1[0-9]):([0-5][0-9])(\s[a|p]m)$/i;
		Ext.apply(Ext.form.field.VTypes, {
		    //  vtype validation function
		    time: function(val, field) {
		        return timeTest.test(val);
		    },
		    // vtype Text property: The error text to display when the validation function returns false
		    timeText: 'Not a valid time.  Must be in the format "12:34 PM".',
		    // vtype Mask property: The keystroke filter mask
		    timeMask: /[\d\s:amp]/i
		});

		// custom Vtype for vtype:'ipaddress'
		Ext.apply(Ext.form.field.VTypes, {
		    ipaddress:  function(val) {
		        return /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(val);
		    },
		    ipaddressText: 'Must be a numeric IP address',
		    ipaddressMask: /[\d\.]/i
		});

		// customer vtype form vtype: 'phone'
		Ext.apply(Ext.form.field.VTypes,{
		    phone: function(val){
		        return /^(\d{3,4}[-]?){1,2}(\d{4})$/.test(val);
		    },
		    phoneText: 'Not a valid phone number.  Must be in the format 021-12345678 or 0812-3456-7890 (dashes optional)',
		    phoneMask: /[\d-]/i
		});

		// customer vtype form vtype: 'npwp'
		Ext.apply(Ext.form.field.VTypes,{
		    npwp: function(val){                
		        return /^\d{2}\.\d{3}\.\d{3}\.\d{1}-\d{3}\.\d{3}$/.test(val);
		    },
		    npwpText: 'Not a valid Tax ID number.  Must be in the format 99.999.999.9-999.999',
		    npwpMask: /[\d-\.]/i
		});

		// customer vtype form vtype: 'trs_no'
		Ext.apply(Ext.form.field.VTypes,{
		    trs_no: function(val){                
		        return /^[A-Z]{2}\d{5}\/\d{4}\/\d{2}\/\d{4}$/.test(val);
		    },
		    trs_noText: 'Not a valid Transaction number.  Must be in the format XX99999/9999/99/9999',
		    trs_noMask: /[A-Z\d\/]+/i
		});

		// display firstscreen		
		var me = this;				
		localStorage.setItem('editFormActive', 'no');	                    

		Ext.Ajax.request({
			url:'php/function/CheckSession.php',
			method: 'POST',
			success: function(conn, response, options, eOpts){
				var result = ExtApp.util.Util.decodeJSON(conn.responseText);
				if (result.success){
					firstScreen = Ext.create('ExtApp.view.main.Main');
				} else {					
					firstScreen = Ext.create('ExtApp.view.login.Login');
				}

				me.add({
					xtype: firstScreen
				});

			},
			failure: function(conn, response, options, eOpts) {
				
				// ExtApp.util.Util.showErrorMsg(conn.responseText);
				switch (action.failureType) {
                    case Ext.form.action.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.action.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.action.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', response.result.msg);
				}
            }
		});

		me.callParent(arguments);
	}
});