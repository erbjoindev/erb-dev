        
Ext.define('ExtApp.view.checkbox.BasicCheckbox', {
    extend: 'Ext.form.field.Checkbox',
     requires: [
    ],
    alias: 'widget.basiccheckbox',
    inputValue:'1',
    boxLabel : '',
    initComponent: function() {
        var me = this;
        me.baseLabel = me.fieldLabel + ":";
        if(me.readOnly==true){
            me.fieldStyle='background-color: #ddd; background-image: none;';
        }else{
            me.fieldStyle='';
        };
        this.callParent(arguments);
    }
});
