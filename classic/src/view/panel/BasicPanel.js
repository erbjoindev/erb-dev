Ext.define('ExtApp.view.panel.BasicPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.basicpanel',
    requires: [
    ],
    layout: {
        type: 'anchor'
    },    
    // border : 0,
    // bodyStyle: 'background-color: transparent;',
    border: '3 0 0 0',
    style: {
        borderColor: '#fff',
        borderStyle: 'solid'
    },
    initComponent: function(){
        var me = this;
        me.callParent(arguments);
    },
    isetReadOnly:function(mode){
        var me=this;
        me.query('[xtype^=textfield]').forEach(
            function(c){                        
                        c.isetReadOnly(mode);
            }
        );

        me.query('[xtype^=textarea]').forEach(
            function(c){                        
                        c.isetReadOnly(mode);
            }
        );
        
		me.query('[xtype^=combo]').forEach(
    	    function(c){
		    			c.isetReadOnly(mode);
			}
    	);
    }
});