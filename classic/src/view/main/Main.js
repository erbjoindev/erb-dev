Ext.define('ExtApp.view.main.Main', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.main',
    requires: [
        'ExtApp.view.main.MainHeader',
        'ExtApp.view.main.MainMenu',
        'ExtApp.view.main.MainFrame'
    ],
    xtype: 'app-main',
    width: 500,
    height: 450,
    layout: 'border',
    controller: 'main',
    items: [
    {
        xtype: 'mainheader',
        region: 'north'
    }, {
        xtype: 'mainmenu',
        region: 'west',
        width: 250,
        split: true
    }, {
        xtype: 'mainframe',
        region: 'center',
        bodyPadding: 0,
        bodyCls: 'panel-background'
    }]
});
