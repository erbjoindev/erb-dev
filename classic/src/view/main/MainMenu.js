Ext.define('ExtApp.view.main.MainMenu', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.mainmenu',
	reference: 'treelistContainer',
	controller: 'mainmenu',
    viewModel: 'mainmenu',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    scrollable: 'y',
    cls: 'treelist-with-nav',
    items: [
        {
            xtype: 'treelist',
            reference: 'treelist',
            bind: '{navItems}',
            expanderFirst: false,
            highlightPath: true,
            ui: 'nav'
        }
    ]
})