Ext.define('ExtApp.view.main.MainHeader', {
	extend: 'Ext.toolbar.Toolbar',
	alias: 'widget.mainheader',
	controller: 'mainheader',
	autoShow: true,
	height: 70,
	ui: 'mainheader',
	initComponent: function(){
		var userName = localStorage.getItem('userName');
		this.items = [
			{
				xtype: 'image',
				height: 60,
				width: 65,
				src: 'resources/images/logo.png'
			},{
				xtype: 'label',
                html: '<div id="titleHeader"><span style="font-size:14px; font-style:italic; color:#fff"><strong>WE DO IT TO HELP YOU</strong></span></div>'
			},{
				xtype: 'tbfill'
			},{
				xtype: 'button',
	            text: userName,
	            iconCls: 'fa fa-cogs',
	            menu: [{
	                text: 'Profile',
	                glyph: 'f007@FontAwesome',
	                id: 'profile'
	            }, {
	                text: 'Logout',
	                glyph: 'f011@FontAwesome',
	                id: 'logout'
	            }]
			}
		]
		this.callParent(arguments);
	}
})