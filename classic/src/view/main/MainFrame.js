Ext.define('ExtApp.view.main.MainFrame',{
	extend: 'Ext.panel.Panel',
	alias: 'widget.mainframe',
	layout: {
		type: 'fit'
	},
	bodyPadding: 5,	
	autoScroll: true
});