Ext.define('ExtApp.view.datefield.BasicDatefield',{
	extend: 'Ext.form.field.Date',
	alias: 'widget.basicdatefield',
	// width: 200,
	maxLength : '12',
	format: 'd/m/Y',
	submitFormat: 'Y-m-d',
	altFormats : 'dmY|d-m-Y|d m Y',
	enforceMaxLenth:true,
	msgTarget: 'side',
	initComponent: function(){
		var me = this;
		me.baseLabel = me.fieldLabel + ":";
		me.baseFieldStyle = me.fieldStyle;

		if(me.textAlign=='right'){
			me.textAlign='text-align:right;';
		}else{
			me.textAlign='text-align:left;';
		}

		if(me.readOnly==true){
			me.hideTrigger=true;
			me.fieldStyle='background-color: #ddd; background-image: none;' + ' ' + me.textAlign;
		}else{
			me.fieldStyle=me.textAlign;
		};

		if(me.allowBlank==false){
			me.afterLabelTextTpl=ExtApp.util.Util.required;
		}else{
			me.afterLabelTextTpl='';
		};


		me.callParent(arguments);
	}, 
	isetAllowBlank:function(pMode){
		var me = this;
		if(pMode===false){
			me.labelEl.update(me.baseLabel + ExtApp.util.Util.required);
		}else{
			me.labelEl.update(me.baseLabel);
		};

		Ext.apply(me,{allowBlank: pMode}, {});
	},
	isetReadOnly:function(pMode){
		var me = this;
		if(pMode==false){
			me.setFieldStyle('background-color: #ffffff; background-image: none;' + ' ' + me.textAlign);
		}else{
			me.setFieldStyle('background-color: #ddd; background-image: none;' + ' ' + me.textAlign);
		};
		me.setReadOnly(pMode);
        me.hideTrigger = pMode;
        me.updateLayout();
	}
});