Ext.define('ExtApp.view.login.Login',{	
	extend: 'Ext.panel.Panel',
	alias: 'widget.login',
	controller: 'login',
	autoShow: true,
	autoScroll: true,
	layout: {
		type: 'center'
	},
	bodyCls: 'panel-background',
	items: 
	[
		{
			xtype: 'panel',
			width: 500,
			frame: true,
			layout: {
				type: 'vbox',
				align: 'center',
				pack: 'start'
			},
			bodyPadding: '15 0 75 0',
			items: [
				{
					xtype: 'panel',
					layout: {
						type: 'hbox',
						align: 'center'
					},
					items: [
						{
							xtype: 'image',
							src: 'resources/images/logo.png',
							width: 80,
							height: 80,
							margin: '0 20 0 0'
						},{
							xtype: 'label',
							html: '<div id="titleHeader"><span style="font-size:14px; font-style:italic; color:#32404e"><strong>WE DO IT TO HELP YOU</strong></span></div>'
						}
					]
				},
				{
					xtype: 'form',			
					items: [
						{
							xtype: 'textfield',
							emptyText: 'Username',
							itemId: 'username',
							width: 400,
							height: 50,
							allowBlank: false,					
							msgTarget: 'qtip',
							maxLength: '30',
							enforceMaxLength: true
						},
						{
							xtype: 'textfield',
							inputType: 'password',
							emptyText: 'Password',
							id: 'password',
							itemId: 'password',
							name: 'password',
							width: 400,
							height: 50,
							allowBlank: false,
							margin: '0 0 25 0',
							msgTarget: 'qtip',
							maxLength: '50',
							enforceMaxLength: true,
							enableKeyEvents: true
						}
					],
					dockedItems: [
						{
							xtype: 'button',
							itemId: 'signin',
							dock: 'bottom',
							text: 'Sign In',
							width: 400,
							height: 50,
							formBind: true,					
							scale: 'large'
						}
					]
				}
			]
		}
	]

});