Ext.define('ExtApp.view.trigger.TriggerProfile',{
	extend: 'Ext.form.field.Text',
	alias: 'widget.triggerprofile',

	requires: [
		'ExtApp.view.lov.Lov'
	],
	maxWidth: 180,
	minWidth: 180,        
	fieldLabel: 'Profile',   
    msgTarget: 'qtip',
    vtype: 'numberonly',
    triggers: {
        search: {
            cls: 'fa fa-search',
                handler: function(){                     	
                    var myLov = Ext.create('ExtApp.view.lov.Lov',{
                    title: 'Profile',
                    lovType: 'profile',
                    initField: this,
                    targetField: this
                });
                myLov.show();
            }
        }
    },

    initComponent: function(){
        var me = this;
        if(me.allowBlank==false){
            me.afterLabelTextTpl= ExtApp.util.Util.required
        } else {
            me.afterLabelTextTpl ='';
        }
        me.callParent(arguments);
    },

    isetAllowBlank:function(pMode){
        var me = this;
        if(pMode===false){
            me.labelEl.update(me.baseLabel + ExtApp.util.Util.required);
        }else{
            me.labelEl.update(me.baseLabel);
        };

        Ext.apply(me,{allowBlank: pMode}, {});
    },
    
    isetReadOnly:function(pMode){
        var me = this;
        if(pMode==false){
            me.setFieldStyle('background-color: #ffffff; background-image: none;' + ' ' + me.textAlign);
        }else{
            me.setFieldStyle('background-color: #ddd; background-image: none;' + ' ' + me.textAlign);
        };
        me.setReadOnly(pMode);
    }  
});