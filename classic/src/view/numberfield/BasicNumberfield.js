Ext.define('ExtApp.view.numberfield.BasicNumberfield',{
	extend: 'Ext.form.field.Number'
	, alias: 'widget.basicnumberfield'
	, enforceMaxLenght: true
	, msgTarget: 'qtip'
	, selectOnFocus: true
	, hideTrigger: true	
	, initComponent: function(){
		var me = this;

		me.textAlign='text-align:right;';
		

		if(me.readOnly==true){
            me.fieldStyle='background-color: #ddd; background-image: none;' + ' ' + me.textAlign;
        }else{
            me.fieldStyle=me.textAlign;
        };

        if(me.allowBlank==false){
            me.afterLabelTextTpl=ExtApp.util.Util.required;
        }else{
            me.afterLabelTextTpl='';
        };

		me.callParent(arguments);
	},

	isetAllowBlank:function(pMode){
        var me = this;
        if(pMode===false){
            me.labelEl.update(me.baseLabel + ExtApp.util.Util.required);
        }else{
            me.labelEl.update(me.baseLabel);
        };

        Ext.apply(me,{allowBlank: pMode}, {});
    },
    isetReadOnly:function(pMode){
        var me = this;
        if(pMode==false){
            me.setFieldStyle('background-color: #ffffff; background-image: none;' + ' ' + me.textAlign);
        }else{
            me.setFieldStyle('background-color: #ddd; background-image: none;' + ' ' + me.textAlign);
        };
        me.setReadOnly(pMode);
    }
 //    ,

 //    currencySymbol: null,
	// useThousandSeparator: true,
	// thousandSeparator: ',',
	// alwaysDisplayDecimals: false,

	// setValue: function(v){
	//    Nice.view.numberfield.BasicNumberfield.superclass.setValue.call(this, v);
       
	//    this.setRawValue(this.getFormattedValue(this.getValue()));
 //    },

 //    getFormattedValue: function(v){
       
	// 	if (Ext.isEmpty(v) || !this.hasFormat()) 
 //            return v;
	//     else 
 //        {
	// 		var neg = null;
			
	// 		v = (neg = v < 0) ? v * -1 : v;	
	// 		v = this.allowDecimals && this.alwaysDisplayDecimals ? v.toFixed(this.decimalPrecision) : v;
			
	// 		if(this.useThousandSeparator)
	// 		{
	// 			if(this.useThousandSeparator && Ext.isEmpty(this.thousandSeparator))
	// 				throw ('NumberFormatException: invalid thousandSeparator, property must has a valid character.');
				
	// 			if(this.thousandSeparator == this.decimalSeparator)
	// 				throw ('NumberFormatException: invalid thousandSeparator, thousand separator must be different from decimalSeparator.');
				
	// 			var v = String(v);
		
	// 			var ps = v.split('.');
 //                ps[1] = ps[1] ? ps[1] : null;
                
 //                var whole = ps[0];
                
 //                var r = /(\d+)(\d{3})/;

	// 			var ts = this.thousandSeparator;
				
 //                while (r.test(whole)) 
 //                    whole = whole.replace(r, '$1' + ts + '$2');
            
	// 		    v = whole + (ps[1] ? this.decimalSeparator + ps[1] : '');
	// 		}
			
	// 		return String.format('{0}{1}{2}', (neg ? '-' : ''), (Ext.isEmpty(this.currencySymbol) ? '' : this.currencySymbol + ' '), v);
 //        }
 //    },

	// parseValue: function(v){
	// 	//Replace the currency symbol and thousand separator
 //        return Nice.view.numberfield.BasicNumberfield.superclass.parseValue.call(this, this.removeFormat(v));
 //    },

	// removeFormat: function(v){
 //        if (Ext.isEmpty(v) || !this.hasFormat()) 
 //            return v;
 //        else 
 //        {
	// 		v = v.replace(this.currencySymbol + ' ', '');
			
	// 		v = this.useThousandSeparator ? v.replace(new RegExp('[' + this.thousandSeparator + ']', 'g'), '') : v;
	// 		//v = this.allowDecimals && this.decimalPrecision > 0 ? v.replace(this.decimalSeparator, '.') : v;
			
 //            return v;
 //        }
 //    },

	// getErrors: function(v){
 //        return Nice.view.numberfield.BasicNumberfield.superclass.getErrors.call(this, this.removeFormat(v));
 //    },

	// hasFormat: function()
	// {
	// 	return this.decimalSeparator != '.' || this.useThousandSeparator == true || !Ext.isEmpty(this.currencySymbol) || this.alwaysDisplayDecimals;	
	// },

	// onFocus: function(){
	// 	this.setRawValue(this.removeFormat(this.getRawValue()));
 //    }

});