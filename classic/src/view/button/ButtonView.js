Ext.define('ExtApp.view.button.ButtonView',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonview'
	, glyph: 'f0f6@FontAwesome'
	, text: 'View'
    , itemId: 'btnView'
    , width: 90
});

