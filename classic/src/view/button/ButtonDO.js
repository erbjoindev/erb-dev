Ext.define('ExtApp.view.button.ButtonDO',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttondo'
	, glyph: 'f0d1@FontAwesome'
	, text: 'Generate Delivery Order'
    , itemId: 'btnDO'
    , width: 90
});

