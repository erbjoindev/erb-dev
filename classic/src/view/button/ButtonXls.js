Ext.define('ExtApp.view.button.ButtonXls',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonxls'
	, glyph: 'f1c3@FontAwesome'
	, text: 'XLS'
	, itemId: 'btnXls'
	, width: 90
});