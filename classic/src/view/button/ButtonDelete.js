Ext.define('ExtApp.view.button.ButtonDelete',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttondelete'
	, glyph: 'f056@FontAwesome'
	, text: 'Delete'
	, itemId: 'btnDelete'
	, width: 90
});