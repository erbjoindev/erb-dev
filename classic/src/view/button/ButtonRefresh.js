Ext.define('ExtApp.view.button.ButtonRefresh',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonrefresh'
	, glyph: 'f021@FontAwesome'
	, text: 'Refresh'
	, itemId: 'btnRefresh'
	, width: 90
});