Ext.define('ExtApp.view.button.ButtonCancel',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttoncancel'
	, glyph: 'f00d@FontAwesome'
	, text: 'Cancel'
	, itemId: 'btnCancel'
	, width: 90
});