Ext.define('ExtApp.view.button.ButtonEdit',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonedit'
	, glyph: 'f044@FontAwesome'
	, text: 'Edit'
	, itemId: 'btnEdit'
	, width: 90
});
