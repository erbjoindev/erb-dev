Ext.define('ExtApp.view.button.ButtonSubmit',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonsubmit'
	, glyph: 'f1d8@FontAwesome'
	, text: 'Submit'
    , itemId: 'btnSubmit'
    , width: 90
});

