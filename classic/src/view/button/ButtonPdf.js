Ext.define('ExtApp.view.button.ButtonPdf',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonpdf'
	, glyph: 'f1c1@FontAwesome'
	, text: 'PDF'
	, itemId: 'btnPdf'
	, width: 90
});