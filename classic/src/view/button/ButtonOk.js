Ext.define('ExtApp.view.button.ButtonOk',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonok'
	, glyph: 'f00c@FontAwesome'
	, text: 'OK'
	, itemId: 'btnOk'
	, width: 90
});