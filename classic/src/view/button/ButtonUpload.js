Ext.define('ExtApp.view.button.ButtonUpload',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonupload'
	, glyph: 'f093@FontAwesome'
	, text: 'Upload'
	, itemId: 'btnUpload'
	, width: 90
});

