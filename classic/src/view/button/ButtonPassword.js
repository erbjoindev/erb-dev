Ext.define('ExtApp.view.button.ButtonPassword',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonpassword'
	, glyph: 'f084@FontAwesome'
	, text: 'Reset'
	, itemId: 'btnPassword'
	, width: 90
});