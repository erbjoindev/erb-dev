Ext.define('ExtApp.view.button.ButtonSave',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonsave'
	, glyph: 'f0c7@FontAwesome'
	, text: 'Save'
	, itemId: 'btnSave'
	, width: 90
});

