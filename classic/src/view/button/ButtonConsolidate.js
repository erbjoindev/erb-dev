Ext.define('ExtApp.view.button.ButtonConsolidate',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonconsolidate'
	, glyph: 'f247@FontAwesome'
	, text : 'Consolidate'
	, itemId : 'btnConsolidate'
	, width: 90
});