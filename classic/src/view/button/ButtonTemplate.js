Ext.define('ExtApp.view.button.ButtonTemplate',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttontemplate'
	, glyph: 'f02d@FontAwesome'
	, text: 'Template'
	, itemId: 'btnTemplate'
	, width: 90
});