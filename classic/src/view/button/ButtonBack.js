Ext.define('ExtApp.view.button.ButtonBack',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonback'
	, glyph: 'f0e2@FontAwesome'
	, text: 'Back'
	, itemId: 'btnBack'
	, width: 90
});