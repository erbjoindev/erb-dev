Ext.define('ExtApp.view.button.ButtonAllocate',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonallocate'
	, glyph: 'f0ca@FontAwesome'
	, text : 'Allocate'
	, itemId : 'btnAllocate'
	, width: 90
});