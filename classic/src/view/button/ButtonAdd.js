Ext.define('ExtApp.view.button.ButtonAdd',{
	extend: 'Ext.button.Button'
	, alias: 'widget.buttonadd'
	, glyph: 'f055@FontAwesome'
	, text: 'Add'
	, itemId: 'btnAdd'
	, width: 90
});