Ext.define('ExtApp.view.security.User',{
	extend: 'ExtApp.view.panel.BasicPanel',
	alias: 'widget.user',
	requires: [
		'ExtApp.view.security.UserList'
	],
	controller: 'user',
	viewModel: 'user',
	autoShow: true,
	layout:{
		type:'fit'
	},
	title: 'User',
	tools: [
		{
			itemId: 'ireset'
			, glyph: 'f0e2@FontAwesome'
            , tooltip: 'Reset'
            , html: 'Reset'
            , handler: 'onClickButtonReset'
		},{
			itemId: 'iclose'
			, glyph: 'f00d@FontAwesome'
            , tooltip: 'Close'
            , html: 'Close'
            , handler: 'onClickButtonClose'
		}
	],
	initComponent: function(application) {
		var me = this;
		me.items = [
			{
				xtype: 'userlist',
				bind: '{userList}',
				dockedItems: [
					{
						xtype:'toolbar',
						dock:'top',
						ui:'footer',
						autoScroll:true,
						items:[
							{
								xtype: 'buttonrefresh'
							},
							{
								xtype: 'buttonadd'
							},
							{
								xtype: 'buttonedit',
								bind: {
									disabled: '{disabledButtonEdit}'
								}
							},
							{
								xtype: 'buttonpassword',
								bind: {
									disabled: '{disabledButtonEdit}'
								}
							}
						]

					},
					{
						xtype: 'fieldset',
                        dock: 'top',
                        title: 'Filter',
                        autoScroll: true,
                        layout: {
                            type: 'hbox',
                            align: 'top',
                            pack: 'start'
                        },
                        items:[
                        	{
                        		xtype: 'fcuser'
                        	},
                    		{ 
	                            xtype: 'comboactive',
	                            labelAlign:'right',
	                            margin: '0 0 0 15'
                        	}
                    	]
					},
					{
						xtype: 'mypagingtoolbar',
						bind: {
		                    store: '{userList}'
		                }
					}
				]
			},
			{
				xtype: 'window',
				title: 'Hello',
				height: 200,
				width: 400,
				layout: 'fit',
				items: {
					xtype: 'grid',
					border: false,
					columns: [{header: 'World'}]
				},
				id: 'windows'
			}
		]


		me.callParent(arguments);
	}
})