Ext.define('ExtApp.view.security.UserList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.userlist',

    columnLines: true,
    scrollable:true,
    autoScroll: true,  
    enableColumnMove:false,
    enableColumnHide: false,
    columns: [
        {
            dataIndex: 'musr_code',            
            text: 'Username',
            sortable: false,
            align: 'left',
            width: 150  
        },
        {
            dataIndex: 'musr_name',            
            text: 'Full Name',
            sortable: false,
            align: 'left',
            width: 250
        },
        {
            dataIndex: 'mupf_name',            
            text: 'Profile', 
            sortable: false,
            align: 'left',           
            width: 200
        },
        {
            dataIndex: 'musr_email',            
            text: 'Email',     
            sortable: false,
            align: 'left',       
            width: 250
        },
        {
            dataIndex: 'musr_language_name',            
            text: 'Language',
            sortable: false,
            align: 'left',
            width: 100
        },
        {            
            dataIndex: 'musr_active_name',            
            text: 'Active', 
            sortable: false,
            align: 'left',           
            width: 60
        },
        {            
            dataIndex: 'musr_update_date',            
            text: 'Last Update',
            sortable: false,
            align: 'left',
            width: 200
      
        }
    ], 

    viewConfig: { 
        getRowClass: function(record, index){            
            if(record.get('musr_active') === 0)
            {
                return 'red-line' ;    
            }else {
                return ;
            } 
        },
        
        enableTextSelection: true        
    }

});
