Ext.define('ExtApp.view.grid.BasicGrid', {
    extend: 'Ext.grid.Panel',
    alias : 'widget.basicgrid',
    columnLines: true,        
    autoScroll:true,    
    enableColumnMove:false,
    enableColumnHide: false,

    selModel:{
    	type: 'cellmodel'
    },
    viewConfig:{
    		stripeRows: true,
    		loadMask: true,
            enableTextSelection: true 
    },
    frame : true,
    initComponent: function() {
        var me= this;

        if(me.useCellEdit){
	        me.plugins= [
	                  Ext.create('Ext.grid.plugin.CellEditing', {
	                      clicksToEdit: 1
	                 })        
	        ];    
        }
        this.callParent(arguments);
    }, 
    icheckDuplicate:function(pColumn, pRecord){
        var me = this;
        var duplicate = false;
        me.store.each(function(record, id){ 
            if(me.store.indexOf(record)!=me.store.indexOf(pRecord)){
                if(record.get(pColumn)==pRecord.get(pColumn)){
                    duplicate = true;
                }
            }
        });
        return duplicate;
    },
    icheckDuplicate2:function(pColumn1, pColumn2,pRecord){
        var me = this;
        var duplicate = false;
        me.store.each(function(record, id){ 
            if(me.store.indexOf(record)!=me.store.indexOf(pRecord)){
                if(record.get(pColumn1)==pRecord.get(pColumn1) && record.get(pColumn2)==pRecord.get(pColumn2) ){
                    duplicate = true;
                }
            }
        });
        return duplicate;
    },
    isetDisabled:function(pDisable){        
        this.setDisabled(pDisable);
    }
});