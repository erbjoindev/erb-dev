Ext.define('ExtApp.view.general.DateFieldRange',{
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.datefieldrange',
    requires: [
        'Ext.picker.Date'
        , 'ExtApp.util.Util'
    ],
    layout: {
        type: 'hbox'
    }
    // ,hideLabel : true
    ,fieldLabel: 'Period'
    ,width: 350
    ,initComponent: function() {
        var me = this;
        
        if (me.iitemIdStart==undefined){
            me.iitemIdStart='startDate';
        }

        if (me.iitemIdEnd ==undefined)
            me.iitemIdEnd='endDate';

        if(me.iallowBlank==undefined){
            me.iallowBlank=true;
        }

        if(me.iminValue==undefined){
            me.iminValue='';
        }

        if(me.idefaultDay==undefined){
            me.idefaultDay='';
        }

        if(me.iallowBlank==false){
            me.afterLabelTextTpl= ExtApp.util.Util.required
        } else {
            me.afterLabelTextTpl ='';
        }

        me.items = [
            {
                xtype : 'datefield'
                , hideLabel : true
                , itemId : me.iitemIdStart
                , allowBlank : me.iallowBlank
                , vtype: 'daterange'
                , endDateField: '#'+ me.iitemIdEnd
                , parent: me
                , msgTarget: 'qtip'
                , minValue: me.iminValue
                , value: me.idefaultDay
                , format: 'd/m/Y'
                , submitFormat: 'Ymd'
                , width: 100
            }
            ,{
                xtype : 'tbspacer'
                ,width : 10
            }
            ,{
                xtype : 'label'
                ,text : ' To '
            }
            ,{
                xtype : 'tbspacer'
                ,width : 10
            }
            ,{
                xtype : 'datefield'
                ,hideLabel : true
                ,itemId : me.iitemIdEnd
                ,allowBlank : me.iallowBlank
                ,vtype: 'daterange'
                ,startDateField: '#'+ me.iitemIdStart
                ,parent: me
                ,msgTarget: 'qtip'
                // ,value: me.idefaultDay
                ,format: 'd/m/Y'
                ,submitFormat: 'Ymd'
                ,width: 100
            }
        ];

        me.callParent(arguments);
    }	
});