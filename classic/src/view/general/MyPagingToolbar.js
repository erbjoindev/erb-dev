Ext.define('ExtApp.view.general.MyPagingToolbar',{
	extend: 'Ext.toolbar.Paging',
	alias: 'widget.mypagingtoolbar',	
    dock: 'bottom',    
    displayInfo: true, 
    displayMsg: 'Displaying records {0} - {1} of {2}',
    emptyMsg: 'No records to display'
});