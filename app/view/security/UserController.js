Ext.define('ExtApp.view.security.UserController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.user',
	requires: [
		'ExtApp.util.Util',
		'ExtApp.model.security.User'
	],
	views: [
		'user'
	],
	init: function(application){
		this.control({
			'user': {
				afterrender: this.onFormAfterRender
			},
			'user buttonrefresh': {
				click: this.onClickButtonRefresh
			},
			'user userlist': {
				select: this.onSelectUserList
			},
			'user mypagingtoolbar': {
				beforechange: this.onBeforeChangePagingToolbar
			}
		})
	},

	onFormAfterRender: function(panel){
		me 			= this;
		view 		= me.getView();
		viewModel 	= me.getViewModel();
		me.loadData(true);
	},

	onClickButtonRefresh: function(button){
		me.loadData(false);
	},

	onSelectUserList: function(grid, record, index, eOpts){
		viewModel.set('record', record);
	},

	onBeforeChangePagingToolbar: function(button){
		me.loadData(true);
	},

	onClickButtonClose: function(button){
		ExtApp.util.Util.closePanel();
	},

	loadData: function(pagingtoolbar){
		var record 		= new ExtApp.model.security.User();
		var userList 	= viewModel.get('userList');
		var musr_code 	= view.down('triggeruser').getValue();
		var musr_active = view.down('comboactive').getValue();
		var params 		= {
			musr_code	: musr_code,
			musr_active	: musr_active
		};
		if(pagingtoolbar){
			userList.proxy.extraParams = params;
		} else {
			userList.load({
				params: params
			});
		}
		viewModel.set('record', record);
	}
})