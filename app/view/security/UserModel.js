Ext.define('ExtApp.view.security.UserModel', {
	extend: 'Ext.app.ViewModel',
	alias: 'viewmodel.user',
	data: {
		record: ''
	},
	formulas: {
		disabledButtonEdit: function(get){
			if( get('record.musr_code') == null ||
				get('record.musr_code') == undefined){
				return (true);
			} else {
				return (false);
			}
		}
	},
	stores: {
		userList: {
			storeId: 'usermodel',
			model: 'ExtApp.model.security.User',
			proxy: {
				type: 'ajax',
				url: 'php/security/User.php',
				reader: {
					type: 'json',
					rootProperty: 'data'
				}
			},
			autoLoad: true
		}
	}
})