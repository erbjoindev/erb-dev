Ext.define('ExtApp.view.main.MainHeaderController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.mainheader',
	requires: [
		'ExtApp.util.Util'
	],
	views: [
		'mainheader'
	],
	init: function(application){
		this.control({
			'mainheader #logout': {
				click: this.onClickLogout
			}
		})
	},

	onClickLogout: function(button){
		Ext.Ajax.request({
			url: 'php/login/Logout.php',
			method: 'POST',
			success: function(conn, response, options, eOpts){

                var result = ExtApp.util.Util.decodeJSON(conn.responseText);

                if (result.success) {
                    
					localStorage.removeItem('userCode');
					localStorage.removeItem('userName');
					localStorage.removeItem('userProfile');  
					localStorage.removeItem('userLanguage');
					localStorage.removeItem('editFormActive');    

					/*var lovScreen = Ext.ComponentQuery.query('lov')[0];
					if (lovScreen!==null && lovScreen !== undefined){
						lovScreen.destroy(); 
					}*/

					var mainScreen = button.up('app-init');
					mainScreen.down('main').destroy();

					var panelLogin = Ext.create('ExtApp.view.login.Login');
					mainScreen.add({
						xtype: panelLogin
					}) 

                } else {

                    ExtApp.util.Util.showErrorMsg(result.msg); 
                }
            },
            failure: function(conn, response, options, eOpts) {
                
                ExtApp.util.Util.showErrorMsg(conn.responseText);
            }
		});
	}
})