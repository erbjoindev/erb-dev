Ext.define('ExtApp.view.main.MainMenuModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.mainmenu',

    stores: {
        navItems: {
            type: 'tree',
            storeId: 'mainmenu',
            model: 'ExtApp.model.main.MainMenu',
            proxy: {
                type: 'ajax',
                url: 'php/main/mainmenu.php',
                reader: {
                    type: 'json',
                    rootProperty: 'items'
                }
            },
            autoLoad:true
        }
    }
});