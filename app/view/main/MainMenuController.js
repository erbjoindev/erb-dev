Ext.define('ExtApp.view.main.MainMenuController', {
	extend: 'Ext.app.ViewController',
	alias: 'controller.mainmenu',
	requires: [
		'ExtApp.util.Util'
	],
	views: [
		'mainmenu'
	],
	init: function(application){
		this.control({
			'mainmenu treelist': {
				selectionchange : this.onItemSelected
			}
		})
	},

	onItemSelected: function (sender, record, eOpts) {
		if(sender.getSelection() != null){
			if (record.raw.className !== null && record.raw.className !== undefined && record.raw.className !== "")	{
				ExtApp.util.Util.displayOnMainFrame(record.raw.className);
			}
		}
    }
})