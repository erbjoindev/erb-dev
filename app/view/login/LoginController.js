Ext.define('ExtApp.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',
    requires: [
        'ExtApp.util.MD5',
        'ExtApp.view.main.Main',
        'ExtApp.util.Util',
        'ExtApp.util.Alert',
        'ExtApp.util.SessionMonitor'
    ],

    views: [
        'ExtApp.view.login.Login'        
    ],
 
    init: function(application){
        this.control({
            "login form textfield": {
                specialkey: this.onTextfieldSpecialKey
            },

            "login form button#signin": {
                click: this.onButtonClickSignin
            }
        });
    },

    onTextfieldSpecialKey: function(field, e, options) {
        if (e.getKey() == e.ENTER){
            var submitBtn = field.up('form').down('button#signin');
            submitBtn.fireEvent('click', submitBtn, e, options);
        }
    },

    onButtonClickSignin: function(pButton) {
        
        var mainScreen = pButton.up('app-init');
        var panelLogin = mainScreen.down('login').down('form'),
        user = panelLogin.down('#username').getValue(),
        pass = panelLogin.down('#password').getValue();

        // check user ke server
        if (panelLogin.getForm().isValid()){
            pass = ExtApp.util.MD5.encode(pass);
            Ext.get(mainScreen.getEl()).mask("Authenticating...please wait",'loading');
            Ext.Ajax.request({
                url: 'php/login/login.php',
                method: 'POST',
                params: {
                    username: user,
                    password: pass
                },
                success: function(conn, response, options, eOpts){
                    Ext.get(mainScreen.getEl()).unmask();                    
                    var result = ExtApp.util.Util.decodeJSON(conn.responseText);

                    if (result.success) {
                        ExtApp.util.Alert.msg("Welcome!", result.musr_name);
                        
                        localStorage.setItem('userCode', result.musr_code);
                        localStorage.setItem('userName', result.musr_name);
                        localStorage.setItem('userProfile', result.musr_mupf_code);
                        localStorage.setItem('userLanguage', result.musr_language);
                        // close login screen
                        mainScreen.down('login').destroy();

                        //  buka screen utama
                        var mainPanel = Ext.create('ExtApp.view.main.Main');
                        
                        mainScreen.add({
                            xtype: mainPanel
                        })                                                
                    } else {                                                
                        ExtApp.util.Util.showErrorMsg(result.msg);
                    }
                },
                failure: function(conn, response, options, eOpts) {

                    Ext.get(mainScreen.getEl()).unmask();
                
                    ExtApp.util.Util.showErrorMsg(conn.responseText);
                }
            });
        }
    }

});