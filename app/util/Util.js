Ext.define('ExtApp.util.Util', {

    statics : {

        required: '<span style="color:red;font-weight:bold" data-qtip="Required"> *</span>',

        decodeJSON: function (text) {

            var result = Ext.JSON.decode(text, true);

            if (!result){
                result = {};
                result.success = false;
                result.msg = text;
            }

            return result;
        },

        displayOnMainFrame: function(pView){
            var me = this;
            var isFormActive = localStorage.getItem('editFormActive');
            var mainScreen = Ext.ComponentQuery.query('app-init')[0];            
            var mainFrame = mainScreen.down('mainframe');

            if (isFormActive==='yes'){
                Ext.Msg.show({
                    title: 'Confirmation', 
                    msg: 'You have unsaved changes, are you sure you want to leave this page?',
                    buttons: Ext.Msg.YESNO,
                    icon: Ext.Msg.QUESTION,
                    fn: function(buttonId){
                        if (buttonId=='no'){
                            return false;
                        } else {
                            me.removeAllFromMainFrame();

                            mainFrame.add({
                                xtype: pView
                            });     
                        }
                    }
                });
                
            } else {
                me.removeAllFromMainFrame();

                mainFrame.add({
                    xtype: pView
                });     
            }
        },

        closePanel: function(){
            var treelist = Ext.ComponentQuery.query('treelist')[0];
            ExtApp.util.Util.removeAllFromMainFrame();
            treelist.setSelection(null);
        },

        removeAllFromMainFrame: function(){
            var mainScreen = Ext.ComponentQuery.query('app-init')[0];            
            var mainFrame = mainScreen.down('mainframe');

            mainFrame.items.each(
                function(Cmp){
                    mainFrame.remove(Cmp,true);
                }
            );
            localStorage.setItem('editFormActive', 'no');
        },

        showErrorMsg: function (text) {

            Ext.Msg.show({
                title: "Error",
                msg: text,
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK
            });
        },


        showWarningMsg: function(text) {
            Ext.Msg.show({
                title: 'Warning',
                msg: text,
                icon: Ext.Msg.WARNING,
                buttons: Ext.Msg.OK
            });
        },

        showInfoMsg: function(text) {
            Ext.Msg.show({
                title: 'Info',
                msg: text,
                icon: Ext.Msg.INFO,
                buttons: Ext.Msg.OK
            });
        }, 

        recordsToJson: function(records){
            var selected = [];
            Ext.each(records, function (item) {
              // console.log(item.data);
              selected.push(item.data);
            }); 

            return Ext.JSON.encode(selected);
        },

        gridToJson: function(pGrid)
        {
            var parms = []; 
            for (var i = 0; i < pGrid.store.data.items.length; i++) {
                 var xRecord = pGrid.store.data.items [i];
                 parms.push(xRecord.data);
            }           
            
            return Ext.JSON.encode(parms);
        },

        getMarginPct: function(pSellingPrice, pPurchasePrice, pVatCode){
            if(pVatCode==1){
                return (((pSellingPrice/1.1)-pPurchasePrice)/(pSellingPrice/1.1))*100;
            }
        },

        remUndefined: function(pInput){
            if(pInput==undefined){
                return '';
            }else{
                return pInput;
            }
        },

        getData: function(pType, pKey1, pKey2, pKey3, pKey4 ){
            var dataR;

            Ext.Ajax.request({
                waitTitle: 'Connecting',
                waitMsg: 'Sending data...',                                     
                url : 'php/function/getdata.php',
                method: 'GET',
                params : {
                    pType : pType,
                    pKey1 : pKey1, 
                    pKey2 : pKey2, 
                    pKey3 : pKey3,
                    pKey4 : pKey4
                },
                async: false,
                success: function(conn, response, options, eOpts) {
                    dataR = ExtApp.util.Util.decodeJSON(conn.responseText);  
                }
            });
            return dataR;
        },
        getDataAsync:function(pType, pKey1, pKey2, pKey3, pTarget, pColumn ){
                            var dataR;
                            var me = this;

                            Ext.Ajax.request({
                            waitTitle: 'Connecting',
                            waitMsg: 'Sending data...',                                     
                              url : 'php/function/getdata.php',
                              method: 'GET',
                              params : {
                               pType : pType,
                               pKey1 : pKey1, 
                               pKey2 : pKey2, 
                               pKey3 : pKey3 
                              },
                              async: true,
                              success: function(conn, response, options, eOpts) {
                                dataR = ExtApp.util.Util.decodeJSON(conn.responseText);  
                                if(dataR.success){
                                    pTarget.setValue(dataR.data[pColumn]);
                                }else{
                                    pTarget.setValue('');
                                }
                              }
                             });
        },

        addDate:function(pDate, pDay){
            var result =  new Date(pDate.getTime() + (pDay * 24 * 3600 * 1000));
            return result;
        },

        dateDiff: function(pDate1, pDate2){
            var MS_PER_DAY = 1000 * 60 * 60 * 24;
            var utc1 = Date.UTC(pDate1.getFullYear(), pDate1.getMonth(), pDate1.getDate());
            var utc2 = Date.UTC(pDate2.getFullYear(), pDate2.getMonth(), pDate2.getDate());

            return Math.floor((utc2 - utc1) / MS_PER_DAY);
        }

        , randomPassword: function(){
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for( var i=0; i < 8; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }
    }
});