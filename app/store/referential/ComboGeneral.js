Ext.define('ExtApp.store.referential.ComboGeneral', {
	extend: 'Ext.data.Store',
	requires: [
		'ExtApp.model.referential.ComboGeneral'
	],	
	model: 'ExtApp.model.referential.ComboGeneral',
	remoteSort: false,
	proxy   : {
		type : 'ajax',
		url  : 'php/referential/combogeneral.php',
		reader: {
			type: 'json',
			rootProperty : 'data'
		}
	},
	autoLoad: false
});
