Ext.define('ExtApp.store.security.ParameterTableHdr', {
    extend: 'Ext.data.Store',
    requires: [
        // model name
        'ExtApp.model.security.ParameterTableHdr'
    ],
    model: 'ExtApp.model.security.ParameterTableHdr',
    proxy: {
        type: 'ajax',
        url: 'php/security/ParameterTableHdr.php',
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});