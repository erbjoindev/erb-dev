Ext.define('ExtApp.store.security.User', {
    extend: 'Ext.data.Store',
    alias: 'store.user',

    requires: [
        'ExtApp.model.security.User'
    ],

    model: 'ExtApp.model.security.User',
    proxy: {
        type: 'ajax',
        url: 'php/security/User.php',   
        
        reader: {
            type: 'json', 
            rootProperty: 'data'            
        }
    }
});