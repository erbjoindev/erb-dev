Ext.define('ExtApp.store.security.Profile', {
    extend: 'Ext.data.Store',
    alias: 'store.profile',
    requires: [
        'ExtApp.model.security.Profile'
    ],
    model: 'ExtApp.model.security.Profile',
    proxy: {
        type: 'ajax',
        url: 'php/security/Profile.php',
        
        reader: {
            type: 'json',
            rootProperty: 'data'
        }
    }
});