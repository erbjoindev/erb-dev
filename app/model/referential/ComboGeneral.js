Ext.define('ExtApp.model.referential.ComboGeneral', {
	extend: 'Ext.data.Model',
	fields: [
		{name: 'code'},
		{name: 'description'}
	]
});