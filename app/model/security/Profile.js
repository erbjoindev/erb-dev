Ext.define('ExtApp.model.security.Profile', {
    extend: 'Ext.data.Model',    
    fields: [
        { name: 'mupf_code' },
        { name: 'mupf_name' },
        { name: 'mupf_update_date', type: 'date', dateFormat: 'Y-m-j H:is'}
    ]
});