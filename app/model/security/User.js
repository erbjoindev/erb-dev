Ext.define('ExtApp.model.security.User', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'musr_code' },
        { name: 'musr_name' },
        { name: 'musr_mupf_code'},
        { name: 'mupf_name'},        
        { name: 'musr_active', type: 'int'},
        { name: 'musr_active_name'},
        { name: 'musr_email'},
        { name: 'musr_language'},
        { name: 'musr_update_date'}        
    ]
});