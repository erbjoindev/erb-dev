Ext.define('ExtApp.model.security.ParameterTableHdr',{
	extend: 'Ext.data.Model',
	fields: [
		{name : 'pthd_no' , type: 'int'},
		{name : 'pthd_access', type: 'int'},
		{name : 'pthd_desc'},
		{name : 'pthd_cmnt'},
		{name : 'pthd_sys', type : 'int'},
		{name : 'pthd_create_date'},
		{name : 'pthd_create_user'},
		{name : 'pthd_upd_date'},
		{name : 'pthd_upd_user'}
	] 
});


