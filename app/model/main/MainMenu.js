Ext.define('ExtApp.model.main.MainMenu',{
	extend: 'Ext.data.Model',	
	fields: [
		{ name: 'id' },
		{ name: 'text' },
		{ name: 'parent_id' },
		{ name: 'leaf' }
	] 
});

