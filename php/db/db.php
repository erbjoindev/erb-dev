<?php 
// UAT
// $server = "10.21.0.109";

// PROD
// $server = "10.21.8.111";

// localhost
$server = "localhost";
// $server = "localhost"; $user = "root"; $pass="";
$user = "root";
$pass =  "";
$dbName = "extdb";

$mysqli = new mysqli($server, $user, $pass, $dbName);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
?>