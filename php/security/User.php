<?php
	
require("../db/db.php");

session_start();

if(isset($_REQUEST['msto_code'])){	
	$msto_code = $_REQUEST['msto_code'];
}

$select = "
	SELECT mst_user.*, mst_user_profile.*,
	IF (
		(`mst_user`.`musr_active` = 1),
		'Yes',
		'No'
	) AS `musr_active_name`,
	IF (
		(`mst_user`.`musr_language` = 1),
		'Bahasa',
		'English'
	) AS `musr_language_name`
 ";

$from = " FROM mst_user 
	INNER JOIN mst_user_profile ON musr_mupf_code = mupf_code ";
$where = "";

$wherecnt = 0;
if(isset($_REQUEST['musr_code']))
{
	if($_REQUEST['musr_code']<>''){
		$musr_code = $_REQUEST['musr_code'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " musr_code = '$musr_code'";
		$wherecnt++;
	}
}

if(isset($_REQUEST['musr_name']))
{
	if($_REQUEST['musr_name']<>''){
		$musr_name = $_REQUEST['musr_name'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " musr_name LIKE '%$musr_name%' ";
		$wherecnt++;
	}
}

if(isset($_REQUEST['musr_mupf_code']))
{
	if($_REQUEST['musr_mupf_code']<>''){
		$musr_mupf_code = $_REQUEST['musr_mupf_code'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " musr_mupf_code = '$musr_mupf_code'";
		$wherecnt++;
	}
}

if(isset($_REQUEST['musr_active']))
{
	if($_REQUEST['musr_active']<>''){
		$musr_active = $_REQUEST['musr_active'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " musr_active = $musr_active";
		$wherecnt++;
	}
}

if(isset($_REQUEST['msto_code']))
{
	if($_REQUEST['msto_code']<>''){
		$msto_code = $_REQUEST['msto_code'];
		$from .= " INNER JOIN mst_user_store ON must_musr_code = musr_code AND must_msto_code = '$msto_code' ";
	}
}

if(isset($_REQUEST['lovSearch']))
{
	if($_REQUEST['lovSearch']<>''){
		$lovSearch = $_REQUEST['lovSearch'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " (musr_code LIKE '$lovSearch' OR musr_name LIKE '$lovSearch' ) ";
		$wherecnt++;
	}
}

if(isset($_REQUEST['lovSearch2']))
{
	if($_REQUEST['lovSearch2']<>''){
		$lovSearch2 = $_REQUEST['lovSearch2'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " (musr_mupf_code = '$lovSearch2') ";
		$wherecnt++;
	}
}

$order = " ORDER BY musr_code ";
$limit = "";

$start = "0";
$lmt = 0;
if(isset($_REQUEST['start'])){
	$start = $_REQUEST['start'];	
}

if(isset($_REQUEST['limit'])){
	$lmt = $_REQUEST['limit'];
	$limit = " LIMIT $start,$lmt ";
}

$sql = $select.$from.$where.$order.$limit;

$result = array();

if($resultdb = $mysqli->query($sql)){
	while($row = $resultdb->fetch_assoc()){
		$result[] = $row;
	}
	$resultdb->close();
}

$sql = "SELECT COUNT(*) as total ".$from.$where;
$total = 0;
if ($resultdb = $mysqli->query($sql)) {
	$row = $resultdb->fetch_assoc();
	$total = $row['total'];
	$resultdb->close();
}

echo json_encode(array(
	"success" => $mysqli->connect_errno == 0,
	"data" => $result,
	"total" => $total
));	

/* close connection */
$mysqli->close();

?>