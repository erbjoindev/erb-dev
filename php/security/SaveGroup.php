<?php

require("../db/db.php");

session_start();

$editmode = $_POST['editmode'];
$mupf_code = $_POST['mupf_code']; 
$mupf_name = $_POST['mupf_name'];
$permissions = $_POST['permissions'];
$permissions = explode(',', $permissions);
$update_user = $_SESSION['musr_code'];	

if ($editmode ==  "new") { //create

	$insertQuery = "INSERT INTO mst_user_profile (mupf_code, mupf_name, mupf_update_user) VALUES ('$mupf_code', '$mupf_name','$update_user')";

	if ($resultdb = $mysqli->query($insertQuery)) {	

		foreach ($permissions as $menu_id) {

			$insertQuery = "INSERT INTO mst_user_privileges (mupv_menu_id, mupv_mupf_code, mupv_update_user) ";
			$insertQuery .= "VALUES ('$menu_id', '$mupf_code', '$update_user')";

			$resultdb = $mysqli->query($insertQuery);
		}		
	}	
} else {

	$updateQuery = "UPDATE mst_user_profile SET mupf_name='$mupf_name', mupf_update_user = '$update_user' WHERE mupf_code='$mupf_code'";

	if ($resultdb = $mysqli->query($updateQuery)) {

		$deleteQuery = "DELETE FROM mst_user_privileges WHERE mupv_mupf_code='$mupf_code'";

		if ($resultdb = $mysqli->query($deleteQuery)) {

			foreach ($permissions as $menu_id) {

				$insertQuery = "INSERT INTO mst_user_privileges (mupv_menu_id, mupv_mupf_code, mupv_update_user) ";
				$insertQuery .= "VALUES ('$menu_id', '$mupf_code', '$update_user')";				

				$resultdb = $mysqli->query($insertQuery);
			}
		}	
	}	
}

function checkChild($mysqli, $menu_id){
	$result = false;
	$query = "SELECT * FROM menu WHERE id = $menu_id AND parent_id IS NOT NULL";
	if($resultdb = $mysqli->query($query)){
		if($resultdb->num_rows > 0){
			$result = true;
		}
	}
	return $result;
}

function saveSubMenu($mysqli, $id, $mupf_code, $update_user){
	$query = "SELECT * FROM menu WHERE parent_id = $id";
	if($resultdb = $mysqli->query($query)){
		if($resultdb->num_rows > 0){
			while($row = $resultdb->fetch_assoc()){
				$insertQuery = "INSERT INTO mst_user_privileges (mupv_menu_id, mupv_mupf_code, mupv_update_user) ";
				$insertQuery .= "VALUES ('".$row['id']."', '$mupf_code', '$update_user')";
				$mysqli->query($insertQuery);
				saveSubMenu($mysqli, $row['id'], $mupf_code, $update_user);
			}
		}
	}
}

echo json_encode(array(
	"success" => $mysqli->error == '',
	"msg" => $mysqli->error
));

/* close connection */
$mysqli->close();

?>