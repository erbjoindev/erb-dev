<?php
require("../db/db.php");

session_start();

$select = " SELECT mccu_musr_code, musr_name as mccu_musr_name, mccu_mcus_code , mcus_company_name as mccu_mcus_name, mcus_address as mccu_mcus_address ";
$from = " FROM mst_customer_customer
	INNER JOIN mst_user ON musr_code = mccu_musr_code
	INNER JOIN mst_customer ON mcus_code = mccu_mcus_code";

$where = "";
$wherecnt = 0;

if(isset($_REQUEST['mccu_musr_code']))
{
	if($_REQUEST['mccu_musr_code']<>''){
		$mccu_musr_code = $_REQUEST['mccu_musr_code'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		}
		$where .= " mccu_musr_code = '$mccu_musr_code'";
		$wherecnt++;
	}
}

$order = " ORDER BY mccu_musr_code ";
$limit = "";

$start = "0";
if(isset($_REQUEST['start'])){
	$start = $_REQUEST['start'];	
}
if(isset($_REQUEST['limit'])){
	$lmt = $_REQUEST['limit'];
	$limit = " LIMIT $start,$lmt ";	
}

$sql = $select.$from.$where.$order.$limit;

$result = array();

if($resultdb = $mysqli->query($sql)){
	while($row = $resultdb->fetch_assoc()){
		$result[] = $row;
	}
	$resultdb->close();
}

$sql = "SELECT COUNT(*) as total ".$from.$where;
$total = 0;
if ($resultdb = $mysqli->query($sql)) {
	$row = $resultdb->fetch_assoc();
	$total = $row['total'];
	$resultdb->close();
}

echo json_encode(array(
	"success" => $mysqli->error =="",
	"data" => $result,
	"total" => $total
	));

$mysqli->close();

?>