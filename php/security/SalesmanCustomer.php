<?php
require("../db/db.php");

session_start();

$select = " SELECT mscu_musr_code, musr_name as mscu_musr_name, mscu_mcus_code , mcus_name as mscu_mcus_name, mcus_address as mscu_mcus_address ";
$from = " FROM mst_salesman_customer
	INNER JOIN mst_user ON musr_code = mscu_musr_code
	INNER JOIN mst_customer ON mcus_code = mscu_mcus_code";

$where = "";
$wherecnt = 0;

if(isset($_REQUEST['mscu_musr_code']))
{
	if($_REQUEST['mscu_musr_code']<>''){
		$mscu_musr_code = $_REQUEST['mscu_musr_code'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		}
		$where .= " mscu_musr_code = '$mscu_musr_code'";
		$wherecnt++;
	}
}

$order = " ORDER BY mscu_musr_code ";
$limit = "";

$start = "0";
if(isset($_REQUEST['start'])){
	$start = $_REQUEST['start'];	
}
if(isset($_REQUEST['limit'])){
	$lmt = $_REQUEST['limit'];
	$limit = " LIMIT $start,$lmt ";	
}

$sql = $select.$from.$where.$order.$limit;

$result = array();

if($resultdb = $mysqli->query($sql)){
	while($row = $resultdb->fetch_assoc()){
		$result[] = $row;
	}
	$resultdb->close();
}

$sql = "SELECT COUNT(*) as total ".$from.$where;
$total = 0;
if ($resultdb = $mysqli->query($sql)) {
	$row = $resultdb->fetch_assoc();
	$total = $row['total'];
	$resultdb->close();
}

echo json_encode(array(
	"success" => $mysqli->error =="",
	"data" => $result,
	"total" => $total
	));

$mysqli->close();

?>