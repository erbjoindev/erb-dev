<?php

require("../db/db.php");

session_start();

$musr_mupf_code ="";
if (isset($_REQUEST['musr_mupf_code']))
{
    $musr_mupf_code = $_REQUEST['musr_mupf_code'];
}

echo "{";
generate_treedata('NULL',$musr_mupf_code,$mysqli);

function generate_treedata($pParentCode,$musr_mupf_code,$dbConn){
    
    $sql = "SELECT id, text, parent_id, 1 as level,className FROM menu WHERE ";
    if ($pParentCode == 'NULL')
    {
        $sql.=" parent_id IS NULL";
    } else {
        $sql.=" parent_id = $pParentCode";
    }
    $sql .="
    ORDER BY
        id ASC";

    if ($result = $dbConn->query($sql)) {
        $cnt = 0;
        echo "'items':[\r\n";
        while ($row = $result->fetch_assoc()){
            $id = $row['id'];       
            $queryString = "SELECT mupv_menu_id menuId FROM mst_user_privileges ";
            $queryString .= "WHERE mupv_mupf_code = '$musr_mupf_code' ";
            $queryString .= "and mupv_menu_id = $id";

            if ($checked = $dbConn->query($queryString)) {
                $chk = $checked->num_rows > 0 ? 'true' : 'false';
            }

            $cnt++;
            if ($cnt>1)
            {
                echo ",\r\n";
            }
            echo "{id: '".$row['id']."', text: '".$row['text']."', parent_id: '".$row['parent_id']."', className:'".$row['className']."', checked: ".$chk;
            if ($row['level'] < 2){
                echo ", expanded: true";
            }
            if (check_child($row['id'],$dbConn) ){
                echo ", leaf: false, \r\n";
                generate_treedata($row['id'],$musr_mupf_code,$dbConn);
            } else {
                echo ", leaf: true}";
            }           
        }
        echo "]}\r\n";
        if($result)
        {
            $result->close();
        }
    }
}

function check_child($pParentCode, $pdbConn){
    $sql = " SELECT * FROM menu WHERE parent_id = '$pParentCode'";
    $cnt = 0;       
    if ($result = $pdbConn->query($sql)) {
        while ($row = $result->fetch_assoc()){
            $cnt++;
        }
    }
    if ($cnt> 0)
    {
        return true;
    } else {
        return false;
    }
}

/* close connection */
$mysqli->close();
?>