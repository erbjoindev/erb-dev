<?php
	
require("../db/db.php");

session_start();

$select = "SELECT pthd_no, pthd_desc, pthd_access, pthd_cmnt, pthd_sys, pthd_create_date, pthd_create_user, pthd_upd_date, pthd_upd_user  ";
$from = " FROM param_table_hdr ";

$where = "";
$wherecnt = 0;
if(isset($_REQUEST['pthd_no']))
{
	if($_REQUEST['pthd_no']<>''){
		$pthd_no=$_REQUEST['pthd_no'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " pthd_no = '$pthd_no' ";
	}
}


if(isset($_REQUEST['lovSearch']))
{
	if($_REQUEST['lovSearch']<>''){
		$lovSearch = $_REQUEST['lovSearch'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " pthd_no LIKE '$lovSearch' OR pthd_desc LIKE '$lovSearch'  ";
		$wherecnt++;
	}
}

if(isset($_REQUEST['pthd_desc']))
{
	if($_REQUEST['pthd_desc']<>''){
		$pthd_desc=$_REQUEST['pthd_desc'];
		if($wherecnt == 0 )
		{
			$where = " WHERE ";
		} else {
			$where .= " AND ";
		}
		$where .= " upper(pthd_desc) like '%".strtoupper($pthd_desc)."%' ";
	}
}

$order = " ORDER BY pthd_no  ";
$limit = "";

$start = "0";
if(isset($_REQUEST['start'])){
	$start = $_REQUEST['start'];	
}

if(isset($_REQUEST['limit'])){
	$lmt = $_REQUEST['limit'];
	$limit = " LIMIT $start,$lmt ";	
}

$sql = $select.$from.$where.$order.$limit;
// echo $sql;
$result = array();

if ($resultdb = $mysqli->query($sql)) {

	while($row = $resultdb->fetch_assoc()) {
		$result[] = $row;
	}	

	$resultdb->close();
}

$select = "SELECT COUNT(*) as total ";
$sql = $select.$from;


$total = 0;
if ($resultdb = $mysqli->query($sql)) {
	$row = $resultdb->fetch_assoc();
	$total = $row['total'];
	$resultdb->close();
}

echo json_encode(array(
	"success" => $mysqli->connect_errno == 0,
	"data" => $result,
	"total" => $total
));	

/* close connection */
$mysqli->close();


?>