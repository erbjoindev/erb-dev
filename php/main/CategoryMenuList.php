<?php
require("../db/db.php");

session_start();

echo "{";
generate_treedata('NULL',$mysqli);

function generate_treedata($pParentCode,$dbConn){
	
	$sql = "SELECT
		mica_code AS id,
		mica_name AS text,
		mica_parent_code AS parent_id,
		mica_level
	FROM
		mst_item_category
	WHERE mica_code <> '9' AND mica_active = '1' AND ";
	if ($pParentCode == 'NULL')
	{
		$sql.=" mica_parent_code IS NULL";
	} else {
		$sql.=" mica_parent_code = $pParentCode";
	}


	$sql .="
	ORDER BY
		mica_code ASC";

	if ($result = $dbConn->query($sql)) {
		$cnt = 0;
		echo "'items':[\r\n";
		while ($row = $result->fetch_assoc()){
			$cnt++;
			if ($cnt>1)
			{
				echo ",\r\n";
			}
			echo "{id: '".$row['id']."', text: '".$row['text']."', parent_id: '".$row['parent_id']."' ";
			if ($row['mica_level'] < 2){
				echo ", expanded: true";
			}
			if (check_child($row['id'],$dbConn) ){
				echo ", leaf: false, \r\n";
				generate_treedata($row['id'],$dbConn);
			} else {
				// echo ", leaf: true, iconCls: 'no-icon'}";
				echo ", leaf: true}";
			}			
		}
		echo "]}\r\n";
		if($result)
		{
			$result->close();
		}
	}
}

function check_child($pParentCode, $pdbConn){
	$sql = " SELECT * FROM mst_item_category WHERE mica_parent_code = '$pParentCode'";
	$cnt = 0;		
	if ($result = $pdbConn->query($sql)) {
		while ($row = $result->fetch_assoc()){
			$cnt++;
		}
	}
	if ($cnt> 0)
	{
		return true;
	} else {
		return false;
	}
}
$mysqli->close();


?>