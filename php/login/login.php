<?php
require("../db/db.php");
session_start();

$username = $_POST['username'];
$password = $_POST['password'];

$username = stripslashes($username);
$password = stripslashes($password);

$username = $mysqli->real_escape_string($username);
$password = $mysqli->real_escape_string($password);
$sql = "SELECT * FROM mst_user WHERE musr_code = '$username' AND musr_password = '$password' ";

$result = array();

if ($resultdb = $mysqli->query($sql)){
// determine number of rows result set
	$count = $resultdb->num_rows;

	// If result matched $userName and $pass, table row must be 1 row
	if($count==1){ 

		// check status active
		while ($record = $resultdb->fetch_assoc()){
			if($record["musr_active"]){
				
			    $_SESSION['musr_code'] = $record["musr_code"];
			    $_SESSION['musr_name'] = $record["musr_name"];
				
				$result['success'] = true;
				$result['msg'] = 'User authenticated!';
				$result['musr_code'] =  $record["musr_code"];
				$result['musr_name'] =  $record["musr_name"];
				$result['musr_mupf_code'] = $record['musr_mupf_code'];
				$result['musr_language'] =  $record["musr_language"];

			} else{
				$result['success'] = false;
				$result['msg'] = 'User is not active. Please contact you Administrator.';
			}
		}
		

	} else {
		
		$result['success'] = false;
		$result['msg'] = 'Incorrect Username or Password.';
	}

	/* close result set */
	$resultdb->close();
}

/* close connection */
$mysqli->close();

//JSON encoding
echo json_encode($result);

?>