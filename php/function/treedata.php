<?php

class TreeData
{
	public $result;
	public $items;
	public $temp;

	private $db;
	private $alt_query;
	private $arr_query;

	private $success;
	private $data;
	private $message;

	function __construct($db)
	{
		$this->db = $db;
		$this->result = array();
		$this->items = array();
		$this->temp = array();
		$this->arr_query = array();
		$this->alt_query = "";

		$this->success = true;
		$this->message = "";
		$this->data = "";
	}

	public function alternative_query($query)
	{
		$this->alt_query = $query;
	}

	public function define_table($table)
	{
		$this->arr_query['table'] = $table;
	}

	public function define_parent_id($parent)
	{
		$this->arr_query['parent_id'] = $parent;
	}

	public function define_id($id)
	{
		$this->arr_query['id'] = $id;
	}

	public function define_text($text)
	{
		$this->arr_query['text'] = $text;
	}

	public function define_opt($opt)
	{
		$this->arr_query['field'][] = $opt;
	}

	public function define_searchFor($field, $value)
	{
		$this->arr_query['search'][] = $field." LIKE '%".$value."%' ";
	}

	public function define_filter($field, $value)
	{
		$this->arr_query['filter'][] = $field." = ".$value;
	}

	public function buildTreeData()
	{
		TreeData::getData(null, 0);
		TreeData::remapping();
		TreeData::clearTempId();
		foreach ($this->items as $item) {
			$this->result['items'][] = $item;
		}
		$this->data = json_encode($this->result);
	}

	public function generateQuery($id, $stat)
	{
		$query = " SELECT ";
		$query .= $this->arr_query['id']." AS id, ";
		$query .= $this->arr_query['parent_id']." AS parent_id, ";
		$query .= $this->arr_query['text']." AS text ";
		if(isset($this->arr_query['field'])){
			foreach ($this->arr_query['field'] as $key => $value) {
				$count = count($this->arr_query['field']);
				$query .= ", ".$value;
			}
		}
		$query .= " FROM ".$this->arr_query['table'];
		if($stat == 0){
			$wherecnt = 0;
			if(isset($this->arr_query['search'])){
				$query .= $wherecnt == 0 ? " WHERE " : " AND ";
				$query .= "(";
				foreach ($this->arr_query['search'] as $key => $value) {
					$count = count($this->arr_query['search']);
					$query .= $count > 0 && $key <> 0 ? " OR " : "";
					$query .= $value;
				}
				$query .= ")";
				$wherecnt++;
			}
			if(isset($this->arr_query['filter'])){
				foreach ($this->arr_query['filter'] as $key => $value) {
					$query .= $wherecnt == 0 ? " WHERE " : " AND ";
					$query .= $value;
					$wherecnt++;
				}
			}
		} else if ($stat == 1) {
			$query .= " WHERE ";
			$query .= $this->arr_query['id']." = '".$id."' ";
		} else if ($stat == 2) {
			$query .= " WHERE ";
			$query .= $this->arr_query['parent_id']." = '".$id."' ";
		}
		$query .= " ORDER BY ".$this->arr_query['id'];
		return $query;
	}

	private function getData($id, $stat)
	{
		$arrChild = array();
		$hasChild = false;
		$query = TreeData::generateQuery($id, $stat);
		if($stat == 0 && $this->alt_query != ""){
			$query = $this->alt_query;
		}
		if($resultdb = $this->db->query($query)){
			$n = 0;
			if($resultdb->num_rows > 0){
				while($row = $resultdb->fetch_assoc()){
					if(!TreeData::checkExistance('id', $row['id'])){
						$n++;
						TreeData::pushToTemp('id', $row['id']);
						$hasChild = TreeData::checkChild($row['id']);
						if($row['parent_id'] != null && $stat != 2){
							if(!TreeData::checkExistance('id', $row['parent_id'])){
								TreeData::getData($row['parent_id'], 1);	
							}
						}
						$row['text'] = $row['id']." - ".$row['text'];
						if($hasChild){
							$row['expanded'] = true;
							$row['leaf'] = false;
							$row['items'] = array();
							if($stat != 1){
								$row['items'] = TreeData::getData($row['id'], 2);
							}
						} else {
							$row['leaf'] = true;
						}
						if($stat == 2){
							$arrChild[] = $row;
						} else {
							$this->items[] = $row;
						}
					}
				}
			} else {
				if($stat == 0){
					$this->success = false;
					$this->message = $this->db->error;
				}
			}
		}
		return $arrChild;
	}

	private function remapping()
	{
		$arritems = $this->items;
		$itemsCnt = count($arritems);
		for($i=0;$i<$itemsCnt;$i++){
			for($j=0;$j<$itemsCnt;$j++){
				TreeData::setmapping($this->items, $i, $j, 0);
			}
		}
	}

	private function setmapping($array, $pKey, $cKey, $n)
	{
		$pid = $this->items[$pKey]['id'];
		if($pid == $this->items[$cKey]['parent_id']){
			$this->items[$pKey]['items'][] = $this->items[$cKey];
			$this->temp['rm_id'][] = $cKey;
		} else {
			if(!$this->items[$pKey]['leaf']){
				for($i=0;$i<count($this->items[$pKey]['items']);$i++){
					$pid = $this->items[$pKey]['items'][$i]['id'];
					if($pid == $this->items[$cKey]['parent_id']){
						$this->items[$pKey]['items'][$i]['items'][] = $this->items[$cKey];
						$this->temp['rm_id'][] = $cKey;
					} else {
						if(!$this->items[$pKey]['items'][$i]['leaf']){
							for($j=0;$j<count($this->items[$pKey]['items'][$i]['items']);$j++){
								$pid = $this->items[$pKey]['items'][$i]['items'][$j]['id'];
								if($pid == $this->items[$cKey]['parent_id']){
									$this->items[$pKey]['items'][$i]['items'][$j]['items'][] = $this->items[$cKey];
									$this->temp['rm_id'][] = $cKey;
								}
							}
						}
					}
				}
			}
		}
	}

	private function clearTempId()
	{
		if(isset($this->temp['rm_id'])){
			for($i=0;$i<count($this->temp['rm_id']);$i++){
				$key = $this->temp['rm_id'][$i];
				if(isset($this->items[$key])){
					unset($this->items[$key]);
				}
			}
		}
	}

	private function checkChild($id)
	{
		$result = false;
		$query = "SELECT * FROM mst_item_category WHERE mica_parent_code = '$id'";
		if($resultdb = $this->db->query($query)){
			$result = $resultdb->num_rows > 0;
		}
		return $result;
	}

	private function checkExistance($key, $value)
	{
		$result = false;
		if(isset($this->temp[$key])){
			foreach ($this->temp[$key] as $item) {
				if($item === $value){
					$result = true;
				}
			}
		}
		return $result;
	}

	private function pushToTemp($key, $value)
	{
		if(!TreeData::checkExistance($key, $value)){
			$this->temp[$key][] = $value;
		}
	}

	public function getResult()
	{
		$result = json_encode(array(
			"success" => $this->success,
			"data" => $this->data,
			"message" => $this->message
			));
		return json_decode($result);
	}
}

?>