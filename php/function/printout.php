<?php

class PrintOut
{
	public $w_row;
	public $w_num;

	function __construct()
	{
		$this->w_num = array();
		$this->w_row = array();
	}

	function DefineWordWrap($key, $value, $max){
		$w_array = explode("\n", wordwrap($value, $max));
		$this->w_row[$key] = $w_array;
		array_push($this->w_num, count($w_array));
	}

	function GetMaxRow(){
		$max = 0;
		foreach ($this->w_num as $item) {
			if($item > $max) {
				$max = $item;
			}
		}
		return $max;
	}

	function validateRow($field, $index){
		$result = '';
		if(isset($this->w_row[$field][$index])){
			$result = $this->w_row[$field][$index];
		} 
		return $result;
	}

	function validateString($value){
		$result = $value;
		if(number_format($value, 0) == 0 || $value == ''){
			$result = '';
		}
		return $result;
	}

	function validateNum($value){
		$result = $value;
		if(number_format($value, 0) == 0 || $value == ''){
			$result = 0;
		}
		return $result;
	}

	function validateDOStatus($value, $status){
		$result = $value;
		if($status == 0){
			$result = '';
		}
		return $result;
	}

	function close(){
		$this->w_num = array();
		$this->w_row = array();
	}
}

?>