<?php

class Pivot
{
	private $mysqli;
	public $result;
	public $num_rows;
	public $x_axisData;
	public $y_axisData;
	public $xy_axisData;
	public $x_field;
	public $y_field;
	public $p_field;
	public $rm_field;

	function __construct($db)
	{
		$this->mysqli = $db;
		$this->result = array();
		$this->x_axisData = array();
		$this->y_axisData = array();
		$this->xy_axisData = array();
		$this->rm_field = array();
		$this->num_rows = 0;
	}

	function generatePivot($query, $addtQuery, $x_field, $y_field, $p_field, $rm_field){
		$this->x_field = $x_field;
		$this->y_field = $y_field;
		$this->p_field = $p_field;
		if($rm_field != null){
			$this->rm_field = $rm_field;
		}
		if(Pivot::getData($query, $addtQuery, 0)){
			Pivot::getData($query, $addtQuery, 1);
			Pivot::getTotal($query);
			Pivot::rebuildData();
		}
	}

	function getData($query, $addtQuery, $axis){
		$result = true;
		if($axis == 0){
			$query .= " GROUP BY ".$this->x_field.", ".$this->y_field." ORDER BY ".$this->x_field.", ".$this->y_field;
		} else {
			$query .= " GROUP BY ".$this->y_field." ORDER BY ".$this->y_field.$addtQuery;
		}
		if($resultdb = $this->mysqli->query($query)){
			if($resultdb->num_rows > 0){
				while ($row = $resultdb->fetch_assoc()) {
					Pivot::registerData($row, $axis);
				}
			} else {
				$result = false;
			}
		} else {
			$result = false;
		}

		return $result;
	}

	function getTotal($query){
		$result = 0;
		$query .= " GROUP BY ".$this->y_field." ORDER BY ".$this->y_field;
		if($resultdb = $this->mysqli->query($query)){
			if($resultdb->num_rows > 0){
				$result = $resultdb->num_rows;
			}
		}
		return $result;
	}

	function rebuildData(){
		foreach ($this->y_axisData as $item) {
			for($i=0;$i<count($this->x_axisData);$i++){
				$x = $this->x_axisData[$i][$this->x_field];
				$y = $item[$this->y_field];
				$item[$this->p_field.$i] = Pivot::getPivotData($x, $y);
			}
			if(count($this->rm_field > 0)){
				foreach ($this->rm_field as $key) {
					unset($item[$key]);
				}
			}
			$this->result[] = $item;
		}
		$this->num_rows = count($this->y_axisData);
	}

	function registerData($data, $axis){
		if($axis == 0){
			if(!Pivot::isExist($this->x_axisData, $data, 0)){
				$this->x_axisData[] = $data;
			}
			$this->xy_axisData[] = $data;
		} else {
			$this->y_axisData[] = $data;
		}
	}

	function isExist($array, $data, $state){
		$count = 0;
		foreach ($array as $item) {
			if($state == 0){
				$dataitem = $data[$this->x_field];
				if($item[$this->x_field] == $dataitem){
					$count++;
				}
			}
		}
		$result = $count > 0 ? true : false;
		return $result;
	}

	function getPivotData($x_value, $y_value){
		$result = "";
		foreach ($this->xy_axisData as $item) {
			if($item[$this->x_field] == $x_value && $item[$this->y_field] == $y_value){
				$result = $item[$this->p_field];
			}
		}
		return $result;
	}

}

?>