<?php

class CsvNsp
{

	const csv_template 	= "../../resources/csvuploads/NSP_TEMPLATE.csv";
	const csv_dir 		= "../../resources/csvuploads/";

	private $file_name;
	private $file_tmp;
	private $file_ext;
	private $file_url;

	private $header;
	private $result;
	private $success;
	private $message;
	private $params;

	private $fieldTemplate;
	private $valueTemplate;

	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
		$this->success = true;
		$this->message = "";
		$this->header = array();
		$this->result = array();
		$this->params = array();
		$this->fieldTemplate = array();
		$this->valueTemplate = array();
	}

	function prepare($file)
	{
		$this->file_name = $file['name'];
		$this->file_tmp = $file['tmp_name'];
		$this->file_ext = end(explode('.', $file['name']));
		$this->file_url = CsvNsp::csv_dir.$file['name'];

		if(CsvNsp::uploadCsv()){
			CsvNsp::readCsv();
		}
	}

	function getResult()
	{
		$result = json_encode(array(
			"success" => $this->success,
			"data" => $this->result,
			"message" => $this->message
			));
		return json_decode($result);
	}

	function uploadCsv()
	{
		if($this->file_name != null && $this->file_tmp != null){
			if(move_uploaded_file($this->file_tmp, $this->file_url)){
				return true;
			}
		}
		return false;
	}

	function readCsv()
	{
		$this->result = array();
		$record = array();
		if(CsvNsp::validateCSVFormatTemplate()){
			$handle = fopen($this->file_url, "r");
			$n = 0;
			while (($data = fgetcsv($handle)) !== false) {
				$line = $n+1;
				if($n !== 0){
					if(CsvNsp::validateCSVFormatRecord($data, $line)){
						foreach ($this->header as $key => $value) {
							$record[$value] = $data[$key];
						}
						if(CsvNsp::validateCSVData($record, $line)) {
							$this->result[] = CsvNsp::rebuild($record);
						} else {
							break;
						}
					} else {
						break;
					}
				}
				$n++;
			}
			fclose($handle);
		} else {
			$this->success = false;
			$this->message .= "Error: ";
			$this->message .= "Invalid csv template!";
		}
	}

	function validateCSVData($data, $line)
	{
		if(!CsvNsp::validateCSVDataType($data, $line)){
			return false;
		} else if(!CsvNsp::validateCSVDuplicateData($data, $line)){
			return false;
		} else {
			foreach ($this->params['msto_code'] as $key => $value) {
				if(!CsvNsp::validateDB($data, $value, $line)){
					return false;
				}
			}
		}
		return true;
	}

	function validateCSVFormatRecord($data, $line)
	{
		if(json_encode($data) === "[null]"){
			$this->success = false;
			$this->message .= "Error at line ".$line.": ";
			$this->message .= "Empty record is not allowed!";
			return false;
		} else if (count($data) !== count($this->header)){
			$this->success = false;
			$this->message .= "Error at line ".$line.": ";
			$this->message .= "Invalid record!";
			return false;
		}
		return true;
	}

	function validateCSVDataType($data, $line)
	{
		if(!ctype_digit(trim($data['Item Code']))){
			$this->success = false;
			$this->message .= "Error at line ".$line.": ";
			$this->message .= "Incorrect data type for column 'Item Code'!";
			return false;
		} else if(!ctype_digit(trim($data['Barcode']))){
			$this->success = false;
			$this->message .= "Error at line ".$line.": ";
			$this->message .= "Incorrect data type for column 'Barcode'!";
			return false;
		} else if(!ctype_digit(trim($data['Price Level']))){
			$this->success = false;
			$this->message .= "Error at line ".$line.": ";
			$this->message .= "Incorrect data type for column 'Price Level'!";
			return false;
		} else if(!ctype_digit(str_replace(",", "", trim($data['Price'])))){
			$this->success = false;
			$this->message .= "Error at line ".$line.": ";
			$this->message .= "Incorrect data type for column 'Price'!";
			return false;
		} 
		// else if(!CsvNsp::validatePrice($data['Price'])){
		// 	$this->success = false;
		// 	$this->message .= "Error at line ".$line.": ";
		// 	$this->message .= "Incorrect currency format for column 'Price'!";
		// 	return false;
		// }
		return true;
	}

	function validateCSVDuplicateData($data, $line)
	{
		foreach ($this->result as $key => $value) {
			if($value['Item Code'] == $data['Item Code']){
				if($value['Barcode'] == $data['Barcode']){
					if($value['Price Level'] == $data['Price Level']){
						$this->success = false;
						$this->message .= "Error at line ".$line.": ";
						$this->message .= "Duplicate barcode in same price level!";
						return false;
					}
				}
			}
		}
		return true;
	}

	function validateDB($data, $msto_code, $line)
	{
		$callSP = "CALL ValidateNSPBarcode(
					'".$data['Item Code']."', 
					'".$data['Barcode']."',
					'".$data['Price Level']."',
					'".$this->params['start_date']."',
					'".$msto_code."',
					@status,
					@message
					)";
		$getStatus = "SELECT @status AS status, @message AS message";
		if($this->db->query($callSP)){
			$resultdb = $this->db->query($getStatus);
			$row = $resultdb->fetch_assoc();
			if($row['status'] !== '0'){
				$this->success = false;
				$this->message .= "Error at line ".$line.": ";
				$this->message .= $row['message'];
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	function validatePrice($number){
		$n = 0;
		$arrNumber = explode(",", $number);
		if($number !== 0 && (count($arrNumber) > 1 || strlen($number) <= 3)){
			foreach ($arrNumber as $key => $value) {
				$len_value = strlen($value);
				if($n == 0 && $len_value > 3){
					return false;
				} else if ($n !== 0 && ($len_value > 3 || $len_value < 3)){
					return false;
				}
				$n++;
			}
			return true;
		} else {
			return false;
		}
	}

	function validateCSVFormatTemplate()
	{
		if(file_exists(CsvNsp::csv_template)){
			$handle = fopen(CsvNsp::csv_template, "r");
			$csvtmp = fgetcsv($handle);
			fclose($handle);
			$handle = fopen($this->file_url, "r");
			$csvupl = fgetcsv($handle);
			$n = 0;
			if(count($csvtmp) === count($csvupl)){
				for($i=0;$i<count($csvtmp);$i++){
					if(isset($csvupl[$i])){
						if($csvtmp[$i] !== $csvupl[$i]){
							return false;
						}
					}
				}
			} else {
				return false;
			}
			$this->header = $csvtmp;
			fclose($handle);
			return true;
		} else {
			return false;
		}
	}

	function getNSPNumber()
	{
		/*$query = "SELECT getNSPNo() as result";
		if($resultdb = $this->db->query($query)){
			$row = $resultdb->fetch_assoc();
			$result = $row['result'];
		} else {
			$result = '';
		}
		return $result;*/

		$result 	= "";
		$cMonth 	= (int)date('m');
		$cYear 		= (int)date('Y');
		$vMonth 	= 0;
		$vYear 		= 0;
		$counter 	= 1;

		$query = "SELECT
			YEAR(tsph_create_date) as year, MONTH(tsph_create_date) as month, RIGHT(tsph_number, 4) AS counter
			FROM trs_normal_selling_price_hdr 
			ORDER BY tsph_create_date 
			DESC, tsph_number DESC limit 0,1
			";
		if($resultdb = $this->db->query($query)){
			if($resultdb->num_rows > 0){
				$row 	 = $resultdb->fetch_assoc();
				$vMonth  = (int)$row['month'];
				$vYear 	 = (int)$row['year'];
				$counter = (int)$row['counter'];
			}
		}
		if($cYear > $vYear || $cMonth > $vMonth){
			$counter = 1;
		} else {
			$counter += 1;
		}
		
		$nsp_number = "NSP/";
		$nsp_number .= $cYear."/";
		$nsp_number .= str_pad($cMonth, 2, '0', STR_PAD_LEFT)."/";
		$nsp_number .= str_pad($counter, 4, '0', STR_PAD_LEFT);

		$result = $nsp_number;
		return $result;
	}

	function addParam($field, $value)
	{
		$this->params[$field] = $value;
	}

	function rebuild($data)
	{
		$data['Price'] = str_replace(",", "", $data['Price']);
		$data['Barcode'] = str_replace(" ", "",trim($data['Barcode']));
		$data['Item Code'] = str_replace(" ", "", trim($data['Item Code']));
		return $data;
	}

	/*function prepareTemplate()
	{
		$max = 10;
		$cntNum = 0;
		$arrVal = array();
		$arrTemp = array();
		$query = "SELECT * FROM param_table_dtl WHERE ptdt_pthd_no = '109'";
		if($resultdb = $this->db->query($query)){
			while ($row = $resultdb->fetch_assoc()) {
				$this->fieldTemplate[] = $row['ptdt_desc'];
				$index = $row['ptdt_desc'];
				$arrVal[$index] = array();
				for($i=1;$i<=$max;$i++){
					if($row['ptdt_v'.$i] != ""){
						$arrVal[$index][] = $row['ptdt_v'.$i];
					}
				}
			}
		}
		foreach ($arrVal as $key => $value) {
			if(count($value) < $max){
				$max = count($value);
			}
		}
		if($max != 0){
			for($i=0;$i<$max;$i++){
				foreach ($this->fieldTemplate as $key => $value) {
					$arrTemp[] = $arrVal[$value][$i];
				}
				CsvNsp::setValueTemplate($arrTemp);
				$arrTemp = array();
			}
			$this->success = true;
		} else {
			$this->success = false;
		}
	}*/

	function getTemplate()
	{
		$content = "";
		foreach ($this->fieldTemplate as $key => $value) {
			$content .= $key == 0 ? $value : ",".$value;
			$content .= $key == (count($this->fieldTemplate) - 1) ? "\r\n" : "";
		}
		foreach ($this->valueTemplate as $keyrow => $row) {
			foreach ($row as $keycol => $column) {
				$content .= $keycol == 0 ? CsvNsp::addQuote($column) : ",".CsvNsp::addQuote($column);
				$content .= $keycol == (count($row) - 1) ? "\r\n" : "";
			}
		}
		if($content != ""){
			CsvNsp::createTemplate($content);
		}
		return $content;
	}

	function createTemplate($content)
	{
		$file = fopen(CsvNsp::csv_template, "w") or die("Unable to open file!");
		fwrite($file, $content);
		fclose($file);
	}

	function setFieldTemplate($field)
	{
		$this->fieldTemplate = $field;
	}

	function setValueTemplate($value)
	{
		$this->valueTemplate[] = $value;
	}

	function addQuote($value)
	{
		if(strpos($value, ",")){
			$value = '"'.$value.'"';
		}
		return $value;
	}

	function close()
	{
		if(file_exists($this->file_url)){
			unlink($this->file_url);
		}
	}
}

?>