<?php

require("../db/db.php");

@session_start();

switch($_REQUEST['pType'])
{
	case "picking_list_hdr" : 

			$sql="
					select tplh_number, 
								 date_format(tplh_date,'%d/%m/%Y') as tplh_date, 
								 tplh_msto_code, 
								 msto_name as tplh_msto_name,
								 tplh_status,
								 ptdt_desc as tplh_status_desc,
								 tplh_create_date, 
								 tplh_create_user,
								 tplh_update_date, 
								 tplh_update_user 
					from trs_picking_list_hdr 
					join mst_store on msto_code = tplh_msto_code 
					left join param_table_dtl on ptdt_pthd_no = '102' and  ptdt_entry_code=tplh_status  
					where tplh_number='".$_REQUEST['pKey1']."' ";
					
			break;			
	case "barcode" : 

			// $sql="
			// 	select mibc_barcode, 
			// 				mibc_msto_code, 
			// 				 mibc_mitm_code,
			// 				 mitm_name as mibc_mitm_name,
			// 				 mibc_unit,
			// 				 mitm_uom,
			// 				 mitm_qtypack,
			// 				 mibc_price 
			// 	from mst_item_barcode 
			// 	join mst_item on mitm_code = mibc_mitm_code and mitm_msto_code = mibc_msto_code 
			// 	where mibc_barcode = '".$_REQUEST['pKey1']."'  
			// 	and mibc_msto_code = '".$_REQUEST['pKey2']."' 
			// 	and mibc_active=1 AND mitm_active = 1 ";


		$sql = "
			SELECT 
				mibc_barcode, 
				mibc_msto_code, 
				mibc_mitm_code,
				mitm_name as mibc_mitm_name,
				mibc_unit,
				mitm_vat,
				mitm_uom,
				mitm_qtypack,
				mitm_scale,
				mitm_avg_cost,
				(tbpr_price + (tbpr_price * mcus_delivery_cost / 100)) as tbpr_price,
				0 as tbpr_discount 
			FROM mst_item 
				JOIN mst_item_barcode ON mibc_mitm_code = mitm_code AND mibc_msto_code = mitm_msto_code
				JOIN trs_barcode_price ON tbpr_mibc_barcode = mibc_barcode AND tbpr_msto_code = mibc_msto_code
				JOIN mst_customer_type ON mcty_mplv_code = tbpr_mplv_code 
				JOIN mst_customer ON mcus_msto_code = mitm_msto_code AND mcus_mcty_code = mcty_code
			WHERE mitm_msto_code = '".$_REQUEST['pKey2']."'
			AND mibc_barcode = '".$_REQUEST['pKey1']."'
			AND mcus_code = '".$_REQUEST['pKey3']."'
			AND (mitm_class = 'ALL' OR mitm_class = mcty_class)
		";
		break;	
	case "barcodediscount":
		/*$sql = "
			SELECT
				max(tbdi_discount_amount) as tbdi_discount_amount
			FROM
				mst_item_barcode
			INNER JOIN trs_barcode_price ON tbpr_msto_code = mibc_msto_code
			AND tbpr_mibc_barcode = mibc_barcode
			INNER JOIN trs_barcode_discount ON tbdi_msto_code = mibc_msto_code
			AND tbdi_mibc_barcode = mibc_barcode
			AND tbdi_mplv_code = tbpr_mplv_code
			WHERE
				mibc_msto_code = '".$_REQUEST['pKey2']."'
			AND mibc_barcode = '".$_REQUEST['pKey1']."'
			AND tbpr_mplv_code = '".$_REQUEST['pKey3']."'
			AND tbdi_qty <= ".$_REQUEST['pKey4']."
			
			AND mibc_barcode = '000027497821'
			AND tbpr_mplv_code = '2'
		";*/
		if(strlen($_REQUEST['pKey4']) > 6) {
			$addition_query = " AND mcus_code = '".$_REQUEST['pKey4']."' 
			GROUP BY tbdi_qty ORDER BY tbdi_discount_amount ASC ";
		} else {
			$addition_query = " AND tbdi_qty <= ".$_REQUEST['pKey4']." 
			GROUP BY tbdi_qty ORDER BY tbdi_discount_amount DESC ";
		}
		$sql = "
			SELECT mitm_name as mibc_mitm_name, mitm_qtypack as mibc_mitm_qtypack,
				mibc_unit, mitm_uom as mibc_mitm_uom, tbdi_qty,
				(tbpr_price + (tbpr_price * mcus_delivery_cost / 100)) as tbpr_price,
				tbdi_discount_amount
			FROM
				mst_item_barcode
			INNER JOIN trs_barcode_price ON tbpr_msto_code = mibc_msto_code
			AND tbpr_mibc_barcode = mibc_barcode
			LEFT JOIN trs_barcode_discount ON tbdi_msto_code = mibc_msto_code
			AND tbdi_mibc_barcode = mibc_barcode
			AND tbdi_mplv_code = tbpr_mplv_code
			INNER JOIN mst_item ON mibc_mitm_code = mitm_code
			INNER JOIN mst_customer ON mcus_msto_code = mitm_msto_code
			WHERE
				mibc_msto_code = '".$_REQUEST['pKey2']."'
			AND mibc_barcode = '".$_REQUEST['pKey1']."'
			AND tbpr_mplv_code = '".$_REQUEST['pKey3']."'
			".$addition_query." 
		";
		break;		
	case "store" : 

			$sql="
					select msto_code, msto_name  
					from mst_store  
					where msto_code='".$_REQUEST['pKey1']."'";
			break;			
	case "vendor" : 
			$sql="
					select mven_code, mven_name
					from mst_vendor 
					where mven_code='".$_REQUEST['pKey1']."' ";
			break;			
	case "customer" : 
			$sql="
					select mcus_code, mcus_name, mcus_mcty_code, mcus_company_name
					from mst_customer  
					where mcus_code='".$_REQUEST['pKey1']."' 
					and mcus_msto_code='".$_REQUEST['pKey2']."' ";
			break;
	case "customergroup" : 
			$sql="
					select mcgr_code, mcgr_name  
					from mst_customer_group
					where mcgr_code='".$_REQUEST['pKey1']."' ";
			break;
	case "customersubgroup" : 
			$sql="
					select mcsg_code, mcsg_name  
					from mst_customer_subgroup
					where mcsg_code='".$_REQUEST['pKey1']."' ";
			break;
	case "customertype" : 
			$sql="
					select mcty_code, mcty_name , mcty_class, mcty_mplv_code
					from mst_customer_type
					where mcty_code='".$_REQUEST['pKey1']."' ";
			break;
	case "customerzone" : 
			$sql="
					select mczo_code, mczo_name  
					from mst_customer_zone
					where mczo_code='".$_REQUEST['pKey1']."' ";
			break;
	case "customerconsolidation" : 
			$sql="
					select mcco_code, mcco_name  
					from mst_customer_consolidation
					where mcco_code='".$_REQUEST['pKey1']."' ";
			break;
	case "customerclass" : 
			$sql="
					SELECT ptdt_entry_code, ptdt_desc
					FROM param_table_dtl
					WHERE ptdt_pthd_no ='13' AND ptdt_entry_code <> 'ALL'
					AND ptdt_entry_code = '".$_REQUEST['pKey1']."' ";
			break;	

	case "item" : 
			$sql="
					select * 
					from mst_item 
					where mitm_code='".$_REQUEST['pKey1']."'
					AND mitm_msto_code = '".$_REQUEST['pKey2']."' ";
			break;	
	case "item_category":
			$sql = " SELECT mica_code, mica_name
						FROM mst_item_category
					WHERE mica_code = '".$_REQUEST['pKey1']."' ";
			break;
	case "itemclass" : 
			$sql="
					SELECT ptdt_entry_code, ptdt_desc
					FROM param_table_dtl
					WHERE ptdt_pthd_no ='13' 
					AND ptdt_entry_code = '".$_REQUEST['pKey1']."' ";
			break;			
	case "subfamily":
			$sql = " SELECT msfm_code, msfm_name
						FROM mst_subfamily
					WHERE msfm_code = '".$_REQUEST['pKey1']."' ";
			break;
	case "department":
		$sql = " SELECT mdep_code, mdep_name
				FROM mst_department
				WHERE mdep_code = '".$_REQUEST['pKey1']."' ";
		break;
	case "user":
		$sql = " SELECT musr_code,musr_name
				FROM mst_user
				WHERE musr_code = '".$_REQUEST['pKey1']."' ";
		break;
	case "profile":
		$sql = " SELECT mupf_code, mupf_name
				FROM mst_user_profile
				WHERE mupf_code = '".$_REQUEST['pKey1']."' ";
		break;
	case "SOItemPivotColumn":
		$addition_query = "";
		if($_REQUEST['pKey2'] != ""){
			$addition_query .= " AND tsoh_status = '".$_REQUEST['pKey2']."' ";
		}
		$sql = "SELECT
		mcus_company_name as tsoh_mcus_name,
		tsoh_mcus_code,
		tsod_mibc_barcode, 
		SUM(tsod_qty) AS tsod_qty
		FROM trs_sales_order_hdr 
		INNER JOIN trs_sales_order_dtl ON tsod_tsoh_id = tsoh_id
		INNER JOIN mst_customer ON tsoh_mcus_code = mcus_code
		AND tsoh_msto_code = mcus_msto_code
		WHERE tsoh_msto_code = '".$_REQUEST['pKey1']."' 
		".$addition_query."
		AND tsoh_date BETWEEN '".$_REQUEST['pKey3']."' AND '".$_REQUEST['pKey4']."'
		GROUP BY tsoh_mcus_code ORDER BY tsoh_mcus_code";
		break;
	case 'quotation':
		$sql = " SELECT COUNT(*) as total FROM trs_sales_quotation_hdr
				WHERE tsqh_mcus_code = '".$_REQUEST['pKey1']."'
				AND tsqh_msto_code = '".$_REQUEST['pKey2']."'
				AND tsqh_status = ".$_REQUEST['pKey3']."
		";
		break;
}

$result = array();
$vTotal=0;
if($resultdb = $mysqli->query($sql)){
	while($row = $resultdb->fetch_assoc()){
		$result[] = $row;
		$vTotal++;
	}
	$resultdb->close();
}

// $vTotal=1;
if($vTotal==0){
	$myData = array('success' => true, 'total' => $vTotal, 'data' => str_replace(']','',str_replace('[','','')), 'sql' => $sql); 
} else if($vTotal==1){
	$myData = array('success' => true, 'total' => $vTotal, 'data' => str_replace(']','',str_replace('[','',$result[0])), 'sql' => $sql); 
} else {
	$myData = array('success' => true, 'total' => $vTotal, 'data' => str_replace(']','',str_replace('[','',$result)), 'sql' => $sql);
}


$locations =(json_encode($myData));

echo $locations;

$mysqli->close();
