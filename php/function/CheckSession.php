<?php

session_start();

require("../function/Function.php");

$result = array();

if (is_session_started() === false){
	$result['success'] = false;
	$result['msg'] = 'Session Expired';	
} else {
	$result['success'] = true;
	$result['msg'] = '';
}

echo json_encode($result);

?>